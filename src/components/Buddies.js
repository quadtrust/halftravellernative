import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ListView } from 'react-native';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { WHITE, BLACK, BODY_COLOR } from '../assets/Colors';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

const followersList = [
  {
    image: require('../assets/images/profile.png'),
    name: 'Sankalp Singha',
    no_follower: 210,
    recent_travel_place: 'Kolkata'
  },
  {
    image: require('../assets/images/profile.png'),
    name: 'Sanjita Singha',
    no_follower: 150,
    recent_travel_place: 'Darjeeling'
  },
  {
    image: require('../assets/images/profile.png'),
    name: 'Mrityunjay',
    no_follower: 200,
    recent_travel_place: 'Shillong'
  },
  {
    image: require('../assets/images/profile.png'),
    name: 'Neeraj',
    no_follower: 200,
    recent_travel_place: 'Shillong'
  },
  {
    image: require('../assets/images/profile.png'),
    name: 'Mrityunjay Upadhay',
    no_follower: 200,
    recent_travel_place: 'Shimla'
  },
  {
    image: require('../assets/images/profile.png'),
    name: 'Mrityunjay',
    no_follower: 200,
    recent_travel_place: 'Shillong'
  },
  {
    image: require('../assets/images/profile.png'),
    name: 'Mrityunjay',
    no_follower: 200,
    recent_travel_place: 'Shillong'
  },
];

class Buddies extends Component {
  // static navigationOptions = {
  //   tabBarLabel: <Text style={{ color: WHITE, fontFamily: 'ubuntuMedium' }}>BUDDIES</Text>
  // };
  constructor(props) {
    super(props);
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
    };
  }
  componentDidMount() {
    this.setState({
      dataSource: ds.cloneWithRows(followersList)
    });
  }
  renderRow(rowData) {
    return (
      <TouchableOpacity
        activeOpacity={0.7} style={styles.cardContainer}
        onPress={() => this.props.navigation.navigate('profile')}
      >
        <View style={styles.card}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={rowData.image} style={styles.profilePic} />
            <View style={{ marginLeft: 7 }}>
              <Text style={styles.userName}>{rowData.name}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.followerText}>{rowData.no_follower} Followers </Text>
                <Text style={{ color: BLACK }}>|</Text>
                <Text style={styles.followerText}> Recently travelled to
                  <Text style={styles.boldFont}> {rowData.recent_travel_place}</Text>
                </Text>
              </View>
            </View>
          </View>
          <View>
            <Icon name='angle-right' color={BLACK} size={40} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.body}>
        <ListView
          dataSource={this.state.dataSource}
          enableEmptySections
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }
}
const styles = {
  body: {
    backgroundColor: BODY_COLOR,
    paddingVertical: 10
  },
  cardContainer: {
     backgroundColor: WHITE,
     margin: 5,
     borderRadius: 3,
     padding: 10
   },
   card: {
     flexDirection: 'row',
     justifyContent: 'space-between',
     alignItems: 'center'
   },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
  },
  userName: {
    fontSize: 15,
    color: BLACK,
    fontFamily: 'ubuntuBold'
  },
  followerText: {
    fontSize: 11,
    color: BLACK,
    fontFamily: 'ubuntuRegular'
  },
  boldFont: {
    fontFamily: 'ubuntuBold',
    fontSize: 12
  }
};
export default Buddies;
