import React from 'react';
import { View, Text } from 'react-native';
import { HEADER_GREY, BLACK } from '../assets/Colors.js';

const HeaderNotification = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.timeText}>{props.date}</Text>
    </View>
  );
};

const styles = {
  container: {
    height: 40,
    backgroundColor: HEADER_GREY,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 200
  },
  timeText: {
    color: BLACK,
    fontFamily: 'ubuntuBold',
    fontSize: 14
  }
};

export default HeaderNotification;
