import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { PhotoGrid } from '../../components/PhotoGrid';
import { WHITE, BLACK, LIGHT_GREY, BLUE } from '../../assets/Colors';

class AboutMe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      edit: false
    };
  }


  render() {
    const computedHeight = (269 + 15) * 8;
    // computedHeight += 450 + 40;
    const photos = [
          { url: 'http://lorempixel.com/400/400/fashion' },
          { url: 'http://lorempixel.com/400/400/city' },
          { url: 'http://lorempixel.com/400/400/people' },
          { url: 'http://lorempixel.com/400/400/nature' },
          { url: 'http://lorempixel.com/400/400/business' },
          { url: 'http://lorempixel.com/400/400/cats' },
          { url: 'http://lorempixel.com/400/400/food' },
          { url: 'http://lorempixel.com/400/400/nightlife' },
          { url: 'http://lorempixel.com/400/400/people' },
          { url: 'http://lorempixel.com/400/400/technics' },
          { url: 'http://lorempixel.com/400/400/transport' },
          { url: 'http://lorempixel.com/400/400/sports' },
          { url: 'http://lorempixel.com/400/400/fashion' },
          { url: 'http://lorempixel.com/400/400/city' },
          { url: 'http://lorempixel.com/400/400/people' },
          { url: 'http://lorempixel.com/400/400/nature' },
          { url: 'http://lorempixel.com/400/400/business' },
          { url: 'http://lorempixel.com/400/400/cats' },
          { url: 'http://lorempixel.com/400/400/food' },
          { url: 'http://lorempixel.com/400/400/nightlife' },
          { url: 'http://lorempixel.com/400/400/people' },
          { url: 'http://lorempixel.com/400/400/technics' },
          { url: 'http://lorempixel.com/400/400/transport' },
          { url: 'http://lorempixel.com/400/400/sports' }
  ];
    return (
      <View style={styles.body1} onLayout={this.props.getTabHeight.bind(this, 'tab3', computedHeight)}>
        <View style={{ backgroundColor: WHITE, padding: 10, margin: 5 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name='smile-o' size={20} color={BLACK} />
            <Text style={[styles.smallMediumFont, { paddingLeft: 5 }]}>About me :</Text>
          </View>
          <TextInput
            multiline
            onContentSizeChange={(event) => {
            this.setState({ height: event.nativeEvent.contentSize.height });
            }}
            onChangeText={(text) => console.log(text)}
            editable={this.state.edit}
            underlineColorAndroid={'transparent'}
            selectionColor={BLACK}
            style={{ height: Math.max(35, this.state.height), fontFamily: 'ubuntuRegular' }}
          />
          <TouchableOpacity onPress={() => this.setState({ edit: true })}>
            <Text style={styles.blueText}>EDIT DETAILS</Text>
          </TouchableOpacity>
        </View>

        <View style={{ backgroundColor: WHITE, padding: 10, margin: 5 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name='picture-o' size={20} color={BLACK} />
            <Text style={[styles.smallMediumFont, { paddingLeft: 5 }]}>Photos :</Text>

          </View>
          <PhotoGrid PhotosList={photos} />
        </View>

      </View>
    );
  }
}

const styles = {
  body1: {
    backgroundColor: LIGHT_GREY,
    paddingVertical: 10
  },
  cardContainer: {
     backgroundColor: WHITE,
     margin: 5,
     borderRadius: 3,
     padding: 10
   },
   card: {
     flexDirection: 'row',
     justifyContent: 'space-between',
     alignItems: 'center'
   },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
  },
  userName: {
    fontSize: 15,
    color: BLACK,
    fontFamily: 'ubuntuBold'
  },
  followerText: {
    fontSize: 11,
    color: BLACK,
    fontFamily: 'ubuntuRegular'
  },
  boldFont: {
    fontFamily: 'ubuntuBold',
    fontSize: 13
  },
  smallMediumFont: {
    fontFamily: 'ubuntuMedium',
    fontSize: 13,
    color: BLACK
  },
  blueText: {
    color: BLUE,
    fontFamily: 'ubuntuMedium',
    fontSize: 14
  }
};

export default AboutMe;
