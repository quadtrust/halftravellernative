import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { WHITE, BLACK, LIGHT_GREY } from '../../assets/Colors';

class ProfileTab extends Component {
  render() {
    let computedHeight = (65 + 15) * 4;
    computedHeight += 450 + 40;
    return (
      <View style={styles.body1} onLayout={this.props.getTabHeight.bind(this, 'tab1', computedHeight)}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.cardContainer}
          onPress={() => this.props.navigation.navigate('profile')}
        >
          <View style={styles.card}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/profile.png')} style={styles.profilePic} />
              <View style={{ marginLeft: 7 }}>
                <Text style={styles.userName}>Sankalp Singha</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.followerText}>256 Followers </Text>
                  <Text style={{ color: BLACK }}>|</Text>
                  <Text style={styles.followerText}> Recently travelled to
                    <Text style={styles.boldFont}>Kolkata</Text>
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <Icon name='angle-right' color={BLACK} size={40} />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} style={styles.cardContainer}>
          <View style={styles.card}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/profile.png')} style={styles.profilePic} />
              <View style={{ marginLeft: 7 }}>
                <Text style={styles.userName}>Sankalp Singha</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.followerText}>256 Followers </Text>
                  <Text style={{ color: BLACK }}>|</Text>
                  <Text style={styles.followerText}> Recently travelled to
                    <Text style={styles.boldFont}>Kolkata</Text>
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <Icon name='angle-right' color={BLACK} size={40} />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} style={styles.cardContainer}>
          <View style={styles.card}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/profile.png')} style={styles.profilePic} />
              <View style={{ marginLeft: 7 }}>
                <Text style={styles.userName}>Sankalp Singha</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.followerText}>256 Followers </Text>
                  <Text style={{ color: BLACK }}>|</Text>
                  <Text style={styles.followerText}> Recently travelled to
                    <Text style={styles.boldFont}>Kolkata</Text>
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <Icon name='angle-right' color={BLACK} size={40} />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} style={styles.cardContainer}>
          <View style={styles.card}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/profile.png')} style={styles.profilePic} />
              <View style={{ marginLeft: 7 }}>
                <Text style={styles.userName}>Sankalp Singha</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.followerText}>256 Followers </Text>
                  <Text style={{ color: BLACK }}>|</Text>
                  <Text style={styles.followerText}> Recently travelled to
                    <Text style={styles.boldFont}>Kolkata</Text>
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <Icon name='angle-right' color={BLACK} size={40} />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} style={styles.cardContainer}>
          <View style={styles.card}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/profile.png')} style={styles.profilePic} />
              <View style={{ marginLeft: 7 }}>
                <Text style={styles.userName}>Raaj Mallik</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.followerText}>256 Followers </Text>
                  <Text style={{ color: BLACK }}>|</Text>
                  <Text style={styles.followerText}> Recently travelled to
                    <Text style={styles.boldFont}>Shimla</Text>
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <Icon name='angle-right' color={BLACK} size={40} />
            </View>
          </View>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = {
  body1: {
    backgroundColor: LIGHT_GREY,
    paddingVertical: 10
  },
  cardContainer: {
     backgroundColor: WHITE,
     margin: 5,
     borderRadius: 3,
     padding: 10
   },
   card: {
     flexDirection: 'row',
     justifyContent: 'space-between',
     alignItems: 'center'
   },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
  },
  userName: {
    fontSize: 15,
    color: BLACK,
    fontFamily: 'ubuntuBold'
  },
  followerText: {
    fontSize: 11,
    color: BLACK,
    fontFamily: 'ubuntuRegular'
  },
  boldFont: {
    fontFamily: 'ubuntuBold',
    fontSize: 12
  }
};

export default ProfileTab;
