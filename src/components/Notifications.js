import React, { Component } from 'react';
import { Text, ScrollView, View, TouchableOpacity, ListView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { YELLOW, RED, WHITE } from '../assets/Colors';
import CommentNotification from '../components/CommentNotification';
import FollowingNotification from '../components/FollowingNotification';
import FavouritedNotification from '../components/FavouritedNotification';
import HeaderNotification from '../components/HeaderNotification';

const DATA = [
  {
    date: '1-1-2000',
    type: 'comment'
  },
  {
    date: '1-1-2000',
    type: 'follow'
  },
  {
    date: '1-1-2000',
    type: 'favourite'
  },
  {
    date: '1-1-2000',
    type: 'favourite'
  },
  {
    date: '1-1-2000',
    type: 'comment'
  },
  {
    date: '10-10-2000',
    type: 'comment'
  },
  {
    date: '1-10-2000',
    type: 'follow'
  },
  {
    date: '10-1-2000',
    type: 'favourite'
  },
  {
    date: '1-10-2000',
    type: 'favourite'
  },
  {
    date: '10-1-2000',
    type: 'comment'
  },
];

class Notifications extends Component {
  static navigationOptions = {
    tabBarLabel: <View>
      <Icon name="bell-o" size={25} color={WHITE} />
      <View
        style={{ width: 15,
        height: 15,
        borderRadius: 15 / 2,
        backgroundColor: RED,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: -5,
        right: -5, }}
      >
        <Text style={{ color: WHITE, fontSize: 10, fontFamily: 'ubuntuMedium' }}>2</Text>
      </View>
    </View>
  };
  constructor(props) {
    super(props);

    const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId];
    const getRowData = (dataBlob, sectionId, rowId) => dataBlob[`${rowId}`];

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
      getSectionData,
      getRowData,
    });

    const { dataBlob, sectionIds, rowIds } = this.formatData(DATA);
    this.state = {
      dataSource: ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds),
    };
  }

  formatData(data) {
    const dateArray = [];

    for (let i = 0; i < data.length; i++) {
      dateArray.push(data[i].date);
    }

    const uniqueDateArray = dateArray.filter((elem, index, self) => {
      return index === self.indexOf(elem);
    });

    const dataBlob = {};
    const sectionIds = [];
    const rowIds = [];

    for (let sectionId = 0; sectionId < uniqueDateArray.length; sectionId++) {
        const currentDate = uniqueDateArray[sectionId];

        const notifications = data.filter((notification) => notification.date === currentDate);

        if (notifications.length > 0) {
          sectionIds.push(sectionId);

          dataBlob[sectionId] = { date: currentDate };

          rowIds.push([]);

          for (let i = 0; i < notifications.length; i++) {
          const rowId = `${sectionId}:${i}`;

          rowIds[rowIds.length - 1].push(rowId);

          dataBlob[rowId] = notifications[i];
        }
        }
    }
    return { dataBlob, sectionIds, rowIds };
  }


  render() {
    return (
      <View>
      <ListView
        style={styles.container}
        stickySectionHeadersEnabled
        dataSource={this.state.dataSource}
        renderRow={(data) => {
          if (data.type === 'comment') {
            return <CommentNotification />;
          } else if (data.type === 'follow') {
             return <FollowingNotification />;
          }
            return <FavouritedNotification />;
        }}
        renderSectionHeader={(sectionData) => <HeaderNotification {...sectionData} />}
      />
      </View>
    );
  }
}


const styles = {
  container: {
    backgroundColor: WHITE,
  },
  notificationsIconConatiner: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -5,
    right: -5,
  }
};

export default Notifications;
