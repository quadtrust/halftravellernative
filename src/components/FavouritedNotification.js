import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { LIGHT_WHITE, BLACK, PINK, WHITE } from '../assets/Colors.js';

class FollowingNotification extends Component {
  render() {
    return (
      <TouchableOpacity style={{ zIndex: -100 }}>
      <View style={[styles.container, { zIndex: -100 }]}>
        <View style={styles.profileImgCircle}>
          <Image
          style={styles.profileImg}
          source={require('../assets/images/profile.png')}
          />
        </View>
        <View style={styles.descriptionPart}>
        <Text style={styles.timeAgo}>3 min ago</Text>
          <Text style={styles.username}>Sankalp Singha</Text>
          <View style={styles.favPlace}>
          <Icon name='heart' size={15} color={PINK} style={styles.heartIcon} />
          <View>
          <Text style={styles.favPlaceLight}>Favourited
               <Text style={styles.favPlaceLight}> Raj </Text>
               s travel story of
               <Text style={styles.favPlaceBold}> Kolkata</Text>
          </Text>
          </View>
          </View>
          <View style={styles.favImageContainer}>
          <Image
          style={styles.squareFavImg}
          source={require('../assets/images/profile.png')}
          />
          <Image
          style={styles.squareFavImg}
          source={require('../assets/images/profile.png')}
          />
          <Image
          style={styles.squareFavImg}
          source={require('../assets/images/profile.png')}
          />
          <Image
          style={styles.squareFavImg}
          source={require('../assets/images/profile.png')}
          />

          </View>
        </View>
      </View>
      </TouchableOpacity>
  );
}
}

const styles = {
  timeAgo: {
    position: 'absolute',
    top: 3,
    right: 15,
    fontSize: 10,
    fontFamily: 'ubuntuRegular'
  },
  favImageContainer: {
    flexDirection: 'row',
    marginTop: 20
  },
  squareFavImg: {
    width: 60,
    height: 60,
    borderRadius: 0,
    marginRight: 6
  },
  favPlaceLight: {
    fontSize: 12,
    fontFamily: 'ubuntuRegular',
    marginRight: 3
  },
  favPlaceBold: {
    fontSize: 12,
    fontFamily: 'ubuntuMedium',
    marginLeft: 5
  },
  favPlace: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  username: {
    fontFamily: 'ubuntuBold',
    fontSize: 14,
    marginBottom: 3,
    color: BLACK
  },
  heartIcon: {
    marginRight: 7,
    marginTop: 3
  },
  container: {
    flexDirection: 'row',
    paddingLeft: 1,
    paddingRight: 1,
    paddingTop: 20,
    backgroundColor: WHITE
  },
  profileImgCircle: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  profileImg: {
    width: 50,
    height: 50,
    borderRadius: 40
  },
  descriptionPart: {
    flex: 4,
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: LIGHT_WHITE
  }
};

export default FollowingNotification;
