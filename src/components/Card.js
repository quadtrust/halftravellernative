import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { WHITE, INSTAGRAM_BACKGROUND, FACEBOOK_BACKGROUND, TWITTER_BACKGROUND, GOOGLE_BACKGROUND } from '../assets/Colors.js';


const SCREEN_WIDTH = Dimensions.get('window').width;

class Card extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const height = this.props.imgheight + this.props.descheight;
    return (
      <View style={[styles.fullCardView, { height }]}>
        <View style={[styles.imagePart, { height: this.props.imgheight }]}>
          <Image
          resizeMode="cover"
          source={require('../assets/images/new-york.jpg')}
          style={[styles.backgroundImg, { height: this.props.imgheight }]}
          >
            <View style={styles.imgInfo}>
              <View style={{ flex: 3, justifyContent: 'center' }}>
              <Text style={styles.imgHeading}>New York </Text>
              <Text style={styles.imgHeading}>470 USD </Text>
              </View>
              <View>
                <Text style={styles.imgSubHeading}>The Effile Tower</Text>
                <Text style={styles.imgDate}>26 / 11/ 2011</Text>
              </View>
            </View>
            <View style={styles.imgIcon}>
              <Icon name='plane' style={styles.icon} />
              <View style={[styles.checkInButton, { marginTop: 10 }]}>
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Text
                  style={{ color: WHITE, textAlign: 'center', fontFamily: 'ubuntuMedium' }}
                  >Check In</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Image>
        </View>
        <View style={{ height: this.props.paddingheight }} />


        <View style={[styles.descriptionPart, { height: this.props.descheight }]}>
          <View style={[styles.whiteBackground, { height: this.props.descheight }]} >
            <View style={styles.postUser}>
              <Image
              source={require('../assets/images/profile.png')}
              style={styles.postUserImg}
              />
              <View style={styles.postUserData}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text
                  style={{ fontSize: 12, fontFamily: 'ubuntuMedium', marginRight: 5 }}
                  >Luke Price</Text>
                  <Text style={{ fontSize: 10, fontFamily: 'ubuntuRegular' }}>Share a comment</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ fontSize: 12, fontFamily: 'ubuntuMedium', marginRight: 5 }}>7:00</Text>
                  <Text style={{ fontSize: 10, fontFamily: 'ubuntuRegular', marginRight: 5 }}>am </Text>
                  <Text style={{ fontSize: 10, fontFamily: 'ubuntuRegular' }}>2 March 2016</Text>
                </View>
              </View>
            </View>

            <View style={[styles.postHeading, { marginTop: 5, marginBottom: 5 }]}>
            <Text style={{ fontSize: 14, fontFamily: 'ubuntuMedium', marginBottom: 3 }}>Kolkata Lake View</Text>
            <Text style={{ fontSize: 12, fontFamily: 'ubuntuRegular' }}>Tollygunge, Kolkata, West Bengal</Text>
            </View>

            <View style={styles.postDescription}>
            <Text style={{ fontSize: 10, fontFamily: 'ubuntuRegular', marginBottom: 5 }}>Labeled as the 'City of Joy', the 'Cultural capital of India', Kolkata (formerly known as Calcutta) is a city with character </Text>

            </View>
            <View style={styles.postImg}>
            <Image
            resizeMode="cover"
            source={require('../assets/images/london.jpg')}
            style={[styles.postImgImg]}
            />
            </View>
            <View style={[styles.postFooter, { padding: 5, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }]}>
              <IonIcon name='md-share' style={{ fontSize: 20 }} />
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                <View style={[styles.socialIcons, styles.facebookColor]}>
                  <Icon name='facebook' style={styles.whiteColor} />
                </View>
                <View style={[styles.socialIcons, styles.twitterColor]}>
                  <Icon name='twitter' style={styles.whiteColor} />
                </View>
                <View style={[styles.socialIcons, styles.googleColor]}>
                  <Icon name='google-plus' style={styles.whiteColor} />
                </View>
                <View style={[styles.socialIcons, styles.instagramColor]}>
                  <Icon name='instagram' style={styles.whiteColor} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  whiteColor: {
    color: WHITE
  },
  facebookColor: {
    backgroundColor: FACEBOOK_BACKGROUND,
  },
  twitterColor: {
    backgroundColor: TWITTER_BACKGROUND
  },
  googleColor: {
    backgroundColor: GOOGLE_BACKGROUND
  },
  instagramColor: {
    backgroundColor: INSTAGRAM_BACKGROUND
  },
  postUser: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingBottom: 5,
    borderColor: '#ddd',

  },
  socialIcons: {
    width: 30,
    height: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  postUserImg: {
    width: 40,
    height: 40,
    borderRadius: 30,
    marginRight: 10
  },
  postUserData: {
    justifyContent: 'space-between'
  },

  fullCardView: {
    width: SCREEN_WIDTH * 0.75,
    alignItems: 'center',
  },
  backgroundImg: {
  width: SCREEN_WIDTH * 0.75,
  zIndex: 10,
  flexDirection: 'row',
  padding: 15,
  paddingTop: 35
},
postImgImg: {
  width: undefined,
  height: 90,

},
postFooter: {
  borderTopWidth: 1,
  marginTop: 10,
  borderColor: '#ddd',
},
imgInfo: {
  flex: 1
},
imgHeading: {
  fontSize: 18,
  color: WHITE,
  fontWeight: 'bold',
  fontFamily: 'ubuntuBold'
},
imgSubHeading: {
  fontSize: 13,
  color: WHITE,
  fontFamily: 'ubuntuMedium',
  fontWeight: '200'
},
imgDate: {
  fontSize: 12,
  color: WHITE,
  fontWeight: '100',
  fontFamily: 'ubuntuRegular'
},
imgIcon: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
},
icon: {
  fontSize: 50,
  color: WHITE
},
checkInButton: {
  width: 100,
  height: 30,
  borderRadius: 30,
  borderColor: '#aaa',
  borderWidth: 1,
  backgroundColor: 'rgba(211, 211, 211, 0.5)',
  justifyContent: 'center',
  alignItems: 'center'
},

whiteBackground: {
  width: SCREEN_WIDTH * 0.75,
  zIndex: 20,
  backgroundColor: WHITE,
  paddingTop: 15,
  paddingLeft: 15,
  paddingRight: 15,
  paddingBottom: 5
}
};

export default Card;
