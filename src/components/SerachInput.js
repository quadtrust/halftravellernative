import React, { Component } from 'react';
import { View, Text, TextInput, Modal, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
  }
  setModalVisible(visible) {
       this.setState({ modalVisible: visible });
   }
  render() {
    return (
      <Modal
       animationType={'slide'}
       transparent
       visible={this.state.modalVisible}
       onRequestClose={() => this.setModalVisible(!this.state.modalVisible)}
      >
          <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.9)' }}>
            <View style={[styles.modalContainer]}>
             <View style={styles.modalHeader}>

               <TouchableHighlight
                onPress={() => {
                 this.setModalVisible(!this.state.modalVisible);
                }}
               >
                <Icon name={'close'} color={'white'} size={20} />
               </TouchableHighlight>
              </View>
              <View style={{ alignItems: 'center' }}>
                <TextInput />
              </View>
            </View>
          </View>
         </Modal>
    );
  }
}

const styles = {
  modalContainer: {
   margin: 20,
   backgroundColor: 'rgba(0,0,0,.9)',
   padding: 15,
   borderWidth: 1,
   borderColor: 'red'
  },
  modalHeader: {
   flexDirection: 'row',
   justifyContent: 'space-between',
   alignItems: 'center'
  },
};

export default SearchInput;
