import * as _ from 'lodash';
import React, { Component } from 'react';
import { View, Image, Dimensions, Modal, TouchableOpacity, ScrollView } from 'react-native';


class PhotoGrid extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            photoUrl: '',
        };
    }

    photoPopupToggle(photoUrl) {
        this.setState({ modalVisible: !this.state.modalVisible, photoUrl });
    }

    renderChunk() {
        const chunk = _.chunk(this.props.PhotosList, 9);

        return chunk.map(
            (chunkItem, index) => {
                const row = _.chunk(chunkItem, 3);

                return row.map(
                    (rowItem, rowIndex) => {
                        return this.renderPhotoRow(rowItem, rowIndex);
                    }
                );
            }
        );
    }
    renderList() {
      const arr = this.props.PhotosList;
      console.log(arr);
      arr.map((item, index) => {
        console.log('listtt', item.url);
        const img = item.url;
        return (<Image
          source={{ uri: img }}
          style={{
              width: Dimensions.get('window').width,
              height: Dimensions.get('window').height,
              resizeMode: 'contain',
              alignSelf: 'center',
          }}
        />);
      });
    }
    renderPhotoRow(rowItem, rowIndex) {
      // console.log(rowIndex);
        if (rowIndex === 0) {
            return this.renderPhotoRow1(rowItem);
        } else if (rowIndex === 1) {
            return this.renderPhotoRow2(rowItem);
        } else if (rowIndex === 2) {
            return this.renderPhotoRow3(rowItem);
        }
    }
    renderPhotoRow1(row) {
        console.log('renderrow1', row);
        return (
            <View key={1} style={styles.alignCenter}>
                {
                    row.map(
                        (item, index) => {
                            return (
                                <View key={index} style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                                    <TouchableOpacity
                                        onPress={() => { this.photoPopupToggle(item.url); }}
                                    >
                                        <Image
                                            source={{ uri: item.url }}
                                            style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]}
                                        />
                                    </TouchableOpacity>
                                </View>
                            );
                        }

                    )
                }

            </View>
        );
    }
    renderPhotoRow2(row) {
      console.log('renderrow2');
        if (row.length === 1) {
          console.log('renderrow2.1');
            return (
                <View key={row[0].url} style={styles.alignCenter}>
                    <View key={row[0].url} style={[styles.expandedView, { borderRadius: this.props.borderRadius }]}>
                        <TouchableOpacity onPress={() => { this.photoPopupToggle(row[0].url); }}>
                            <Image source={{ uri: row[0].url }} style={[styles.ImageStyle, styles.expandedImage, { borderRadius: this.props.borderRadius }]} />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else if (row.length === 2) {
          console.log('renderrow2.2');
            return (
                <View key={row[0].url} style={styles.alignCenter}>
                    <View key={row[0].url} style={[styles.expandedView, { borderRadius: this.props.borderRadius }]}>
                        <TouchableOpacity onPress={() => { this.photoPopupToggle(row[0].url); }}>
                            <Image source={{ uri: row[0].url }} style={[styles.ImageStyle, styles.expandedImage, { borderRadius: this.props.borderRadius }]} />
                        </TouchableOpacity>
                    </View>
                    <View key={row[1].url} style={styles.flexCol}>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity onPress={() => { this.photoPopupToggle(row[1].url); }}>
                                <Image source={{ uri: row[1].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        } else if (row.length === 3) {
          console.log('renderrow2.3');
            return (
                <View key={row[0].url} style={styles.alignCenter}>
                    <View key={row[0].url} style={[styles.expandedView, { borderRadius: this.props.borderRadius }]}>
                        <TouchableOpacity onPress={() => { this.photoPopupToggle(row[0].url); }}>
                            <Image source={{ uri: row[0].url }} style={[styles.ImageStyle, styles.expandedImage, { borderRadius: this.props.borderRadius }]} />
                        </TouchableOpacity>
                    </View>
                    <View key={row[1].url} style={styles.flexCol}>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity onPress={() => { this.photoPopupToggle(row[1].url); }}>
                                <Image source={{ uri: row[1].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity onPress={() => { this.photoPopupToggle(row[2].url); }}>
                                <Image source={{ uri: row[2].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        }
    }
    renderPhotoRow3(row) {
      console.log('renderrow3');
        if (row.length === 1) {
          console.log('renderrow3.1');
            return (
                <View key={row[0].url} style={styles.alignCenter}>
                    <View key={row[0].url} style={styles.flexCol}>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity onPress={() => { this.photoPopupToggle(row[0].url); }}>
                                <Image source={{ uri: row[0].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            );
        } else if (row.length === 2) {
          console.log('renderrow3.2');
            return (
                <View key={row[0].url} style={styles.alignCenter}>
                    <View key={row[0].url} style={styles.flexCol}>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity onPress={() => { this.photoPopupToggle(row[0].url); }}>
                                <Image source={{ uri: row[0].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                        <View key={row[1].url} style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity
                                onPress={() => { this.photoPopupToggle(row[1].url); }}
                            >
                                <Image source={{ uri: row[1].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        } else if (row.length === 3) {
          console.log('renderrow3.3');
            return (
                <View key={row[0].url} style={styles.alignCenter}>

                    <View style={styles.flexCol}>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity
                                onPress={() => { this.photoPopupToggle(row[0].url); }}
                            >
                                <Image source={{ uri: row[0].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.photoView, { borderRadius: this.props.borderRadius }]}>
                            <TouchableOpacity onPress={() => { this.photoPopupToggle(row[1].url); }}>
                                <Image source={{ uri: row[1].url }} style={[styles.ImageStyle, { borderRadius: this.props.borderRadius }]} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[styles.expandedView, { borderRadius: this.props.borderRadius }]}>
                        <TouchableOpacity onPress={() => { this.photoPopupToggle(row[2].url); }}>
                            <Image source={{ uri: row[2].url }} style={[styles.ImageStyle, styles.expandedImage, { borderRadius: this.props.borderRadius }]} />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    }

    renderGrid() {
        return (
            <View>
                {this.renderChunk()}
            </View>
        );
    }

    render() {
        return (
            <View style={[styles.container]}>
                {this.renderGrid()}

                <View>
                    <Modal
                        animationType={'fade'}
                        transparent={false}
                        onRequestClose={() => { }}
                        visible={this.state.modalVisible}
                    >
                        <ScrollView horizontal>
                          <TouchableOpacity onPress={() => { this.photoPopupToggle(); }}>
                              <Image
                                source={{ uri: this.state.photoUrl }}
                                onPress={() => { this.photoPopupToggle(); }}
                                style={{
                                    width: Dimensions.get('window').width,
                                    height: Dimensions.get('window').height,
                                    resizeMode: 'contain',
                                    alignSelf: 'center',
                                }}
                              />
                          </TouchableOpacity>
                        
                        </ScrollView>
                    </Modal>
                </View>

            </View>


        );
    }

}

/*Styles*/

const styles = {

    container: {
        // flex: 1,
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ImageStyle: {
        width: View.width,
        height: 120,
        resizeMode: 'cover'
    },

    flexCol: {
        flexDirection: 'column',
        flex: 1,
        // backgroundColor: 'black'
    },
    alignCenter: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimensions.get('window').width - 20,
        paddingRight: 5
    },

    photoView: {
        height: 120,
        flex: 2,
        backgroundColor: 'gray',
        marginHorizontal: 5,
        marginVertical: 5,
        justifyContent: 'center'
    },
    expandedView: {
        height: 249,
        backgroundColor: 'gray',
        marginHorizontal: 5,
        marginVertical: 5,
        flex: 2
    },
    expandedImage: {
        height: 249,
    },

};


export { PhotoGrid };
