import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TextInput, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { WHITE } from '../assets/Colors.js';

const SCREEN_WIDTH = Dimensions.get('window').width;

class AllCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[styles.searchbarAndIcon, { height: this.props.height }]}>
        <View style={styles.searchbarAndIconInner}>
            <View style={styles.searchBarView}>
              <View style={[styles.searchBarInput]}>
                <TextInput
                  underlineColorAndroid='transparent'
                  placeholder={'Search something'}
                  placeholderTextColor={'#FFF'}
                  style={styles.textInput}
                />
              </View>
            </View>
            <View style={styles.settingIconView}>
              <Icon name='ios-options-outline' style={[styles.whiteColor, styles.icon]} />
            </View>
        </View>
      </View>
    );
  }
}
const styles = {
  whiteColor: {
    color: WHITE
  },
  textInput: {
    color: WHITE,
    fontSize: 12
  },
  icon: {
    fontSize: 25,
  },
  searchbarAndIcon: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchbarAndIconInner: {
    width: SCREEN_WIDTH * 0.90,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  searchBarView: {
    flex: 5
  },
  settingIconView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchBarInput: {
    backgroundColor: '#8888AA',
    borderRadius: 20,
    height: 40,
    paddingLeft: 20
  }
};

export default AllCard;
