import React, { Component } from 'react';
import { View, Text, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ion from 'react-native-vector-icons/Ionicons';
// import { BoxShadow } from 'react-native-shadow';
import MapView from 'react-native-maps';
// import LinearGradient from 'react-native-linear-gradient';
import { YELLOW, WHITE, PINK, GREEN, RED, BLACK, LIGHT_GREY } from '../assets/Colors';

const WIDTH = Dimensions.get('window').width;
class TravelStory extends Component {
  render() {
    const { region } = this.props;
    console.log(region);

    // const shadowOpt = {
		// 	width: 50,
		// 	height: 50,
		// 	color: '#000',
		// 	// border: 2,
		// 	radius: 50 / 2,
		// 	opacity: 0.2,
		// 	x: 2,
		// 	y: 2,
    //   style: { position: 'absolute',
    //   bottom: 50,
    //   right: 10 }
		// };
    return (
      <View style={{ marginHorizontal: 10 }}>
        <ScrollView>
          {/*story of Follwers*/}
          <View>
            <Text style={styles.headingText}>Stories of my followers</Text>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <TouchableOpacity activeOpacity={0.9} onPress={() => this.props.navigation.navigate('profile')}>
                <Image
                  source={require('../assets/images/profile.png')} style={styles.followersImage}
                />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/profile.png')} style={styles.followersImage}
                />
              </TouchableOpacity><TouchableOpacity activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/profile.png')} style={styles.followersImage}
                />
              </TouchableOpacity><TouchableOpacity activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/profile.png')} style={styles.followersImage}
                />
              </TouchableOpacity><TouchableOpacity activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/profile.png')} style={styles.followersImage}
                />
              </TouchableOpacity><TouchableOpacity activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/profile.png')} style={styles.followersImage}
                />
              </TouchableOpacity>
            </ScrollView>
          </View>

          {/*Upcoming travel*/}
          <View>
            <Text style={styles.headingText}>My upcoming trips</Text>
            <View>
              <TouchableOpacity activeOpacity={0.7} style={styles.card}>
                <View style={[styles.cardFirstSection, { height: 180 }]}>

                  <MapView
                    style={{ height: 180, width: WIDTH - 30 }}
                    region={{
                      latitude: 37.78825,
                      longitude: -122.4324,
                      latitudeDelta: 0.015,
                      longitudeDelta: 0.0121,
                    }}
                  />


                  <View style={styles.tripButtonContainer}>

                    <TouchableOpacity style={[styles.tripButton, { backgroundColor: GREEN }]}>
                      <Text style={styles.tripButtonText}>Start Trip</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity style={[styles.tripButton, { backgroundColor: RED }]}>
                      <Text style={styles.tripButtonText}>Cancel Trip</Text>
                    </TouchableOpacity>*/}
                  </View>
                </View>

                <View style={styles.upcomingInfoContainer}>
                  <Image source={require('../assets/images/profile.png')} style={[styles.profileImage]} />
                  <View style={styles.upcomingTravel}>
                    <Text style={styles.travelText}>
                      You have a trip planned for
                      <Text style={styles.travelHeading}> Ahmedabad</Text>
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon name="calendar" size={12} color={BLACK} style={{ paddingRight: 5 }} />
                      <Text style={styles.travelText}>17 July - 24 March</Text>
                    </View>
                    {/*<View style={{ flexDirection: 'row' }}>
                      <Icon name="thumb-tack" size={12} color={BLACK} style={{ paddingRight: 5, paddingTop: 3 }} />
                      <Text style={styles.travelText}>Starting Point:
                        <Text style={styles.travelHeading}> Guwahati</Text> Ending Point:
                        <Text style={styles.travelHeading}> Kolkata</Text>
                      </Text>
                    </View> */}

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon name="eye" size={15} color={BLACK} style={{ paddingRight: 5 }} />
                      <Text style={styles.travelText}><Text>Group</Text> Travel</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
                      <Text style={styles.travelText}>Joined By: </Text>
                      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: 20 }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Image
                            source={require('../assets/images/profile.png')}
                            style={styles.groupImage}
                          />
                          <Image
                            source={require('../assets/images/profile.png')}
                            style={[styles.groupImage, styles.negativeMargin]}
                          />
                          <Image
                            source={require('../assets/images/profile.png')}
                            style={[styles.groupImage, styles.negativeMargin]}
                          />
                          <Image
                            source={require('../assets/images/profile.png')}
                            style={[styles.groupImage, styles.negativeMargin]}
                          />
                          <Image
                            source={require('../assets/images/profile.png')}
                            style={[styles.groupImage, styles.negativeMargin]}
                          />
                        </View>
                        {/*<Ion name="ios-add" size={15} color={BLACK} />
                        <Text style={styles.travelText}>10 Others</Text>*/}
                      </View>
                    </View>

                  </View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={0.7} style={styles.card}>
                <View style={[styles.cardFirstSection, { height: 180 }]}>
                  {/*<LinearGradient
                    colors={['rgba(0, 0, 0, 0.3)', 'rgba(0,0,0, 0.7)', 'rgba(0,0,0, 1)']}
                    style={styles.linearGradient}
                  />*/}
                  <View style={styles.tripButtonContainer}>

                    <TouchableOpacity style={[styles.tripButton, { backgroundColor: GREEN }]}>
                      <Text style={styles.tripButtonText}>Start Trip</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity style={[styles.tripButton, { backgroundColor: RED }]}>
                      <Text style={styles.tripButtonText}>Cancel Trip</Text>
                    </TouchableOpacity>*/}
                  </View>
                </View>

                <View style={styles.upcomingInfoContainer}>
                  <Image source={require('../assets/images/profile.png')} style={[styles.profileImage]} />
                  <View style={styles.upcomingTravel}>
                    <Text style={styles.travelText}>
                      You have a trip planned for
                      <Text style={styles.travelHeading}> Kolkata</Text>
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon name="calendar" size={12} color={BLACK} style={{ paddingRight: 5 }} />
                      <Text style={styles.travelText}>17 July - 24 March</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Icon name="eye" size={15} color={BLACK} style={{ paddingRight: 5 }} />
                      <Text style={styles.travelText}><Text>Solo</Text> Travel</Text>
                    </View>

                  </View>
                </View>
              </TouchableOpacity>

            </View>
          </View>
          {/* end Upcoming travel*/}
          {/* travelstory start */}
          <View>
            <Text style={styles.headingText}>Travel Stories</Text>
            <ScrollView>
              <TouchableOpacity
                activeOpacity={0.9} style={styles.card}
                onPress={() => this.props.navigation.navigate('traveldetails')}
              >
                <View style={styles.cardFirstSection}>
                  <Image source={require('../assets/images/drawercover.png')} style={styles.storyImage} />
                  </View>
                  <View style={styles.storyCardInfoConatiner}>
                    <Image source={require('../assets/images/profile.png')} style={styles.profileImage} />
                    <View style={styles.storyInfo}>
                      <Text style={styles.travelText} numberOfLines={3}>
                        <Text style={styles.travelHeading}>Sankalp Singha </Text>
                        reconciles several touches into a single gesture.
                        <Text style={styles.travelHeading}> Kolkata </Text>
                        makes single-touch gestures resilientreconciles
                        several touches into a single gesture I
                        t makes single-touch gestures resilient.
                      </Text>
                    </View>
                  </View>
                  <TouchableOpacity style={styles.followersContainer}>
                    <Icon name="heart" size={25} style={{ color: PINK }} />
                    <Text style={styles.followerText}>7 Stars</Text>
                  </TouchableOpacity>
                {/*</View>*/}
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={0.9} style={styles.card}>
                <View style={styles.cardFirstSection}>
                  <Image source={require('../assets/images/boat.jpg')} style={styles.storyImage} />

                  </View>
                  <View style={styles.storyCardInfoConatiner}>
                    <Image source={require('../assets/images/profile.png')} style={styles.profileImage} />
                    <View style={styles.storyInfo}>
                      <Text style={styles.travelText} numberOfLines={3}>
                        <Text style={styles.travelHeading}>Sankalp Singha </Text>
                        reconciles several touches into a single gesture.
                        <Text style={styles.travelHeading}> Kolkata </Text>
                        makes single-touch gestures resilientreconciles
                        several touches into a single gesture I
                        t makes single-touch gestures resilient.
                      </Text>
                    </View>
                  </View>
                  <TouchableOpacity style={styles.followersContainer}>
                    <Icon name="heart" size={25} style={{ color: PINK }} />
                    <Text style={styles.followerText}>7 Stars</Text>
                  </TouchableOpacity>
                {/*</View>*/}
              </TouchableOpacity>

            </ScrollView>
          </View>
          {/*Travel Story end*/}
        </ScrollView>

        <TouchableOpacity
          style={styles.createPlan}
          activeOpacity={0.8}
          onPress={() => this.props.navigation.navigate('newplan')}
        >
          <Ion name="ios-add" size={35} color={WHITE} />
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = {
  activeIcon: {
    color: YELLOW,

  },
  inactiveIcon: {
    color: 'white'
  },
  followersImage: {
    width: 60,
    height: 60,
    borderRadius: 15,
    margin: 5,
    borderWidth: 2,
    borderColor: WHITE
  },
  tripButtonContainer: {
    flexDirection: 'row',
    position: 'absolute',
    top: 10,
    right: 10,
    // left: 10,
    // justifyContent: 'space-between',
    alignItems: 'center'
  },
  tripButton: {
    width: 80,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
    borderRadius: 3
  },
  tripButtonText: {
    color: WHITE,
    fontSize: 11,
    fontFamily: 'ubuntuBold',
  },
  upcomingInfoContainer: {
    flexDirection: 'row',
    // backgroundColor: BLACK,
    // position: 'absolute',
    // bottom: -30,
    marginTop: -40,
    padding: 5
  },
  upcomingTravel: {
    flex: 3,
    marginHorizontal: 7,
    marginTop: 35,
  },
  groupImage: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    borderWidth: 1.5,
    borderColor: WHITE,
  },
  negativeMargin: {
    marginLeft: -15,
  },
  card: {
    backgroundColor: WHITE,
    padding: 5,
    marginBottom: 10,
    borderRadius: 5
  },
  cardFirstSection: {
    marginVertical: 7,
    justifyContent: 'center',
  },
  storyImage: {
    width: WIDTH - 30,
    height: 180,
  },
  linearGradient: {
    top: 0,
    left: 0,
    right: 0,
    height: 180,
    position: 'absolute',
    // borderRadius: 3
  },
  storyCardInfoConatiner: {
    // position: 'absolute',
    // left: 10,
    // bottom: 10,
    // right: 35,
    marginTop: -40,
    flexDirection: 'row',
    padding: 5
  },
  storyInfo: {
    marginHorizontal: 7,
    flex: 2,
    marginTop: 35,
    marginRight: 40
  },
  followersContainer: {
    position: 'absolute',
    bottom: 7,
    right: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  followerText: {
    fontSize: 10,
    color: BLACK,
    fontFamily: 'ubuntuBold'
  },
  profileImage: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    borderWidth: 1.5,
    borderColor: WHITE,

  },
  travelHeading: {
    fontSize: 13,
    color: BLACK,
    fontFamily: 'ubuntuBold',
    paddingHorizontal: 5
  },
  travelText: {
    fontSize: 12,
    color: BLACK,
    fontFamily: 'ubuntuRegular',
  },
  createPlan: {
    width: 50,
    height: 50,
    backgroundColor: PINK,
    borderRadius: 50 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 40,
    right: 10,
    elevation: 50
  },
  headingText: {
    fontSize: 15,
    marginBottom: 10,
    marginTop: 20,
    color: 'rgba(0,0,0,.7)',
    fontFamily: 'ubuntuRegular'
  }
};
export default TravelStory;
