import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { LIGHT_WHITE, BLACK, WHITE, PURPLE } from '../assets/Colors.js';

class FollowingNotification extends Component {
  render() {
    return (
      <TouchableOpacity style={{ zIndex: -100 }}>
      <View style={[styles.container, { zIndex: -100 }]}>
        <View style={styles.profileImgCircle}>
          <Image
          style={styles.profileImg}
          source={require('../assets/images/profile.png')}
          />
        </View>
        <View style={styles.descriptionPart}>
          <Text style={styles.username}>Sankalp Singha</Text>
          <View style={styles.follower}>
            <Icon name='child' size={18} color={PURPLE} style={styles.followerIcon} />
            <Text style={styles.followingText}>Started following you.</Text>
          </View>
        </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  followingText: {
    fontFamily: 'ubuntuMedium',
    fontSize: 12,
  },
  follower: {
    flexDirection: 'row',
  },
  followerIcon: {
    marginRight: 5
  },
  username: {
    fontFamily: 'ubuntuBold',
    fontSize: 14,
    marginBottom: 3,
    color: BLACK
  },
  container: {
    flexDirection: 'row',
    paddingTop: 20,
    backgroundColor: WHITE
  },
  profileImgCircle: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  profileImg: {
    width: 50,
    height: 50,
    borderRadius: 40
  },
  descriptionPart: {
    flex: 4,
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: LIGHT_WHITE
  }
};

export default FollowingNotification;
