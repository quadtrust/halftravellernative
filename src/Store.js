import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { logger } from 'redux-logger';
import reducers from './reducers';

const Store = createStore(reducers, {}, compose(
  applyMiddleware(ReduxThunk, logger)
));

export default Store;
