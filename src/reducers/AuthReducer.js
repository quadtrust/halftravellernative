const INITIAL_STATE = {
  user: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'N': {
      return { ...state };
    }
    default:
     return { ...state };
  }
};
