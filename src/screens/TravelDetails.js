import React, { Component } from 'react';
import {
  View, Text, Image, Dimensions, TouchableOpacity, TextInput, ScrollView, ListView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import ImageCarousel from 'react-native-image-carousel';
import { PINK, BLACK, WHITE, PURPLE, ORANGE, BODY_COLOR } from '../assets/Colors';

const urls = [
  'https://d919ce141ef35c47fc40-b9166a60eccf0f83d2d9c63fa65b9129.ssl.cf5.rackcdn.com/images/67003.max-620x600.jpg',
  'https://d919ce141ef35c47fc40-b9166a60eccf0f83d2d9c63fa65b9129.ssl.cf5.rackcdn.com/images/51681.max-620x600.jpg',
  'https://d919ce141ef35c47fc40-b9166a60eccf0f83d2d9c63fa65b9129.ssl.cf5.rackcdn.com/images/66812.max-620x600.jpg',
  'https://myanimelist.cdn-dena.com/s/common/uploaded_files/1438960604-925d1997518b66f8508c749f36810793.jpeg',
];
const WIDTH = Dimensions.get('window').width;
class TravelDetails extends Component {

  static navigationOptions = {
    headerTitle: 'Travel Story',
  };
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      height: 0,
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
    };
  }
  _imageCarousel: ImageCarousel;
  _renderContent = (idx) => {
    return (
      <Image
        style={styles.container}
        source={{ uri: urls[idx] }}
        // resizeMode={'contain'}
      />
    );
  }

  renderSeparator(rowId) {
    if (rowId === this.state.dataSource.getRowCount() - 1) {
       return null;
    }
    return (
      <View key={rowId} style={{ alignSelf: 'center', alignItems: 'center' }}>
        <View style={styles.circle} />
        <View style={[styles.separator, { marginBottom: 2 }]} />
        <View style={[styles.separator, { marginBottom: 2 }]} />
        <View style={styles.separator} />
        <View style={styles.circle} />
      </View>
    );
  }

  renderRow() {
    return (
      <View>
        <View>
          <Image
            source={require('../assets/images/drawercover.png')}
            style={styles.cardImage} blurRadius={7}
          />
        </View>
        <View style={styles.whiteCardContainer}>
          <View style={styles.whiteCard}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row' }}>
                <View style={styles.carIconContainer}>
                  <Icon name='car' size={15} color={WHITE} />
                </View>
                <View style={{ marginLeft: 7 }}>
                  <Text style={styles.cardHeading}>Amta, Howrah</Text>
                  <Text style={styles.timeText}>Started the journey</Text>
                </View>
              </View>
              <Text style={styles.timeText}>3hrs ago</Text>
            </View>
            <Text style={[styles.normalText, { marginVertical: 15 }]}>
              I have started the my journey to the north east, I really hope
              this trip bring me immense pleasure
            </Text>
            <ImageCarousel
              ref={(imageCarousel: ImageCarousel) => {
                this._imageCarousel = imageCarousel;
              }}
              renderContent={this._renderContent}
              // renderHeader={this._renderHeader}
              // renderFooter={this._renderFooter}
            >
            {urls.map((url) => (
              <Image
                key={url}
                style={styles.image}
                source={{ uri: url }}
                // resizeMode={'contain'}
              />
            ))}
          </ImageCarousel>
            <View style={styles.commentLabelContainer}>
              <Text style={styles.boldSmallFont}>2 Comments</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 15 }}>
                <TouchableOpacity>
                  <Icon name='heart' color={PINK} size={15} />
                </TouchableOpacity>
                <Text style={[styles.boldSmallFont, { marginLeft: 7 }]}>54 likes</Text>
              </View>
            </View>

            <View style={styles.divider} />
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../assets/images/profile.png')}
                style={styles.commentProfile}
              />
              <View style={{ flex: 1 }}>
                <TextInput
                  placeholder='wtite comment...'
                  textAlignVertical={'top'}
                  multiline
                  onContentSizeChange={(event) => {
                  this.setState({ height: event.nativeEvent.contentSize.height });
                  }}
                  underlineColorAndroid={'transparent'}
                  selectionColor={BLACK}
                  style={[styles.input, { height: Math.max(35, this.state.height) }]}
                />

                <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                  <Image
                    source={require('../assets/images/profile.png')}
                    style={styles.commentProfile}
                  />
                  <View style={{ flex: 1 }}>
                    <Text style={styles.boldSmallFont}>Bharati Jalotia</Text>
                    <Text style={styles.smallText}>single-touch gestures resilientreconciles
                    several touches into a single gesture</Text>
                  </View>
                </View>

              </View>
            </View>

          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: BODY_COLOR }}>

        <Image source={require('../assets/images/boat.jpg')} style={styles.mainImage} />
        <View style={styles.linearGradientContainer}>
          <LinearGradient
            start={{ x: 0, y: 0.1 }} end={{ x: 0, y: 1 }}
            locations={[0.0, 0.1, 1]}
            colors={['rgba(255, 255, 255, 0.1)',
             'rgba(255, 255, 255, 0.9)', 'rgba(255,255,255, .9)']}
            style={styles.linearGradient}
          >

            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 11 }}>
                <Text style={styles.mainHeading}>Kolkata to Siliguri</Text>
                <Text style={styles.timeText}>4hr 17mts</Text>
                <View style={styles.divider} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={styles.boldSmallFont}>Food & photography</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon name='comments' size={17} color={PURPLE} />
                    <Text style={styles.boldSmallFont}>117</Text>
                  </View>
                </View>
              </View>
              <View style={{ flex: 2 }}>
              <TouchableOpacity style={styles.barButton}>
                <Icon name='bars' color={WHITE} size={20} />
              </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginVertical: 20 }}>
              <Text style={styles.normalText}>I have hinted that I would often jerk poor
                Queequeg from between the whale and the
                I have hinted that I would often jerk poor
              </Text>
            </View>
          </LinearGradient>
        </View>

        <ListView
          dataSource={this.state.dataSource}
          enableEmptySections
          renderRow={(rowData) => this.renderRow(rowData)}
          renderSeparator={(rowId) => this.renderSeparator(rowId)}
          renderFooter={() => <View style={styles.footer} />}
        />

      </ScrollView>
    );
  }
}

const styles = {
  circle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: ORANGE
  },
  footer: {
    height: 56,
    marginTop: -56,
    backgroundColor: BODY_COLOR
  },
  separator: {
    width: 2,
    height: 10,
    backgroundColor: ORANGE,

  },
  mainImage: {
    width: WIDTH,
    height: 200
  },
  linearGradientContainer: {
    height: 120,
    position: 'relative',
    marginBottom: 30
  },
  mainHeading: {
    fontSize: 19,
    fontFamily: 'ubuntuBold',
    color: BLACK
   },
   normalText: {
     fontSize: 13,
     fontFamily: 'ubuntuRegular',
     color: BLACK,
   },
   barButton: {
     elevation: 20,
     width: 45,
     height: 45,
     backgroundColor: '#DA2F1B',
     borderRadius: 45 / 2,
     justifyContent: 'center',
     alignItems: 'center'
   },
   timeText: {
     fontSize: 12,
     fontFamily: 'ubuntuMedium',
     color: BLACK,
   },
   smallText: {
     fontSize: 11,
     fontFamily: 'ubuntuRegular',
     color: BLACK,
   },
   boldSmallFont: {
     fontSize: 13,
     fontFamily: 'ubuntuBold',
     color: BLACK,
   },
   divider: {
     height: 1,
     backgroundColor: 'rgba(0,0,0,.4)',
     marginVertical: 15
   },
   cardImage: {
     width: WIDTH,
     height: 250,
     paddingHorizontal: 20,
     paddingTop: 30
   },
   whiteCardContainer: {
     justifyContent: 'center',
     alignItems: 'center',
     marginTop: -230,
    //  marginBottom: 30
    },
   whiteCard: {
     backgroundColor: WHITE,
     borderRadius: 5,
     padding: 20,
     width: WIDTH / 1.15
   },
   carIconContainer: {
     elevation: 7,
     width: 35,
     height: 35,
     backgroundColor: '#DA2F1B',
     borderRadius: 35 / 2,
     justifyContent: 'center',
     alignItems: 'center'
   },
   commentLabelContainer: {
     flexDirection: 'row',
     alignSelf: 'flex-end',
     paddingTop: 15
   },
   cardHeading: {
     fontSize: 16,
     fontFamily: 'ubuntuRegular',
     color: BLACK
   },
   input: {
     borderColor: 'gray',
     borderWidth: 1,
     fontFamily: 'ubuntuRegular'
   },
   commentProfile: {
     width: 37,
     height: 37,
     marginRight: 7
   },

  linearGradient: {
    position: 'absolute',
    top: -90,
    bottom: 0,
    left: 0,
    right: 0,
    paddingTop: 30,
    paddingHorizontal: 20
  },
  userName: {
    fontSize: 15,
    fontFamily: 'ubuntuBold',
    color: BLACK
  },
  placeName: {
    fontSize: 13,
    fontFamily: 'ubuntuMedium',
    color: BLACK
  },
  descriptionContainer: {
    marginVertical: 15
  },
  descriptionHeading: {
    fontSize: 16,
    fontFamily: 'ubuntuBold',
    color: BLACK
  },
  description: {
    fontSize: 13,
    fontFamily: 'ubuntuRegular',
    color: BLACK,
    paddingTop: 7,
    lineHeight: 20
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  commentInput: {
    borderBottomWidth: 1,
    borderColor: BLACK
  },
  image: {
    marginRight: 5,
    height: 80,
    width: 80
  },
};

export default TravelDetails;
