import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Picker from 'react-native-picker';

import { BLACK, WHITE, BLUE, LIGHT_WHITE, WHITE_OPACITY, FACEBOOK_BACKGROUND,
GOOGLE_BACKGROUND, LIGHT_GREY } from '../assets/Colors';

const SCREEN_WIDTH = Dimensions.get('window').width;


// Labels is optional
const labels = ['MALE', 'FEMALE'];


class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
      human: 'MALE',
      age: 22
    };
  }
showPicker() {
  Picker.init({
      pickerData: labels,
      selectedValue: [this.state.human],
      onPickerConfirm: data => {
        this.setState({
          human: data[0]
        });
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      }
    });
    Picker.show();
}
createData() {
  const age = [];
  for (let i = 22; i <= 65; i++) {
    age.push(i);
  }
  return age;
}
showAgePicker() {
  Picker.init({
      pickerData: this.createData(),
      selectedValue: [this.state.age],
      onPickerConfirm: data => {
        this.setState({
                  age: data[0]
              });
      },
      onPickerCancel: data => {
          console.log(data);
      },
      onPickerSelect: data => {
          console.log(data);
      }
    });
    Picker.show();
}
  render() {
    return (
      <Image
      resizeMode="cover"
      source={require('../assets/images/login-background.jpg')}
      style={styles.backgroundImg}
      >
      <View style={{ marginHorizontal: 50, marginVertical: 10, flex: 1 }}>
        <Image source={require('../assets/images/logo.png')} style={styles.logo} />
        <View style={styles.marginBottom15}>
        <TextInput
          underlineColorAndroid='transparent'
          placeholder={'username'}
          style={styles.inputField}
          placeholderTextColor={LIGHT_WHITE}
        />
        </View>
        <View style={styles.marginBottom15}>

          <TextInput
            underlineColorAndroid={'rgba(0,0,0,0)'}
            secureTextEntry
            placeholder={'password'}
           style={styles.inputField}
           placeholderTextColor={LIGHT_WHITE}
          />
        </View>
        <View style={styles.marginBottom15}>
          <TextInput
            underlineColorAndroid={'rgba(0,0,0,0)'}
            secureTextEntry
            placeholder={'confirm password'}
            style={styles.inputField}
            placeholderTextColor={LIGHT_WHITE}
          />
        </View>

        <View>
          <Text style={{ color: WHITE, fontFamily: 'ubuntuRegular' }}>age</Text>
          <View style={styles.pickerContainer}>
            <TouchableOpacity
              activeOpacity={0.9} style={{ marginTop: 10 }}
              onPress={this.showAgePicker.bind(this, {})}
            >
              <View style={styles.ageBadge}>
                <Text style={styles.pickerText}>{this.state.age}</Text>
                <Icon name='caret-down' size={15} style={{ marginLeft: 10, color: BLACK }} />
              </View>
            </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.9} style={{ marginTop: 10, marginLeft: 20 }}
            onPress={this.showPicker.bind(this, {})}
          >
            <View style={styles.ageBadge}>
            <Text style={styles.pickerText}>{this.state.human}</Text>
            <Icon name='caret-down' size={15} style={{ marginLeft: 10, color: BLACK }} />
            </View>
          </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity activeOpacity={0.7} style={styles.button}>
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>

        <View style={styles.partitionContainer}>
          <View style={styles.whitePartitionLine} />
          <Text style={styles.orText}>OR</Text>
          <View style={styles.whitePartitionLine} />
        </View>
        <View style={{ marginTop: 30, alignSelf: 'center' }}>
          <TouchableOpacity activeOpacity={0.7}>
            <View style={[styles.socialButton, styles.facebookColor]}>
              <Icon name='facebook-official' color={WHITE} size={20} style={styles.icon} />
              <Text style={styles.socialButtonText}>Login with Facebook</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.7}>
            <View style={[styles.socialButton, styles.googleColor]}>
              <Icon name='google-plus-square' color={WHITE} size={20} style={styles.icon} />
              <Text style={styles.socialButtonText}>Login with Google</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Image>
    );
  }
}
const styles = {
  marginBottom15: {
    marginBottom: 15
  },
  backgroundImg: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 90,
    height: 90,
    alignSelf: 'center',
    resizeMode: 'contain',
    // resizeMode: 'center',
    marginBottom: 10
  },
  inputField: {
    borderWidth: 0,
    fontFamily: 'ubuntuRegular',
    color: LIGHT_WHITE,
    fontSize: 12,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    borderRadius: 5,
    paddingLeft: 15,
  },
  pickerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  ageBadge: {
    flexDirection: 'row',
    width: 100,
    height: 30,
    backgroundColor: LIGHT_GREY,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20
  },
  pickerText: {
    fontSize: 13,
    color: BLACK,
    fontFamily: 'ubuntuMedium'
  },

  partitionContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20
   },
   orText: {
     color: WHITE,
     fontFamily: 'ubuntuMedium',
   },
  whitePartitionLine: {
     height: 2,
     width: SCREEN_WIDTH / 2.5,
     backgroundColor: WHITE_OPACITY,
     marginHorizontal: 2
   },
   socialButton: {
     width: 300,
     height: 45,
     flexDirection: 'row',
     justifyContent: 'center',
     alignItems: 'center',
     borderRadius: 5,
     marginBottom: 20
   },
   socialButtonText: {
    color: WHITE,
    fontFamily: 'ubuntuRegular',
   },
   facebookColor: {
     backgroundColor: FACEBOOK_BACKGROUND
   },
   googleColor: {
     backgroundColor: GOOGLE_BACKGROUND
   },
   icon: {
     position: 'absolute',
     left: 30
   },
  button: {
    backgroundColor: BLUE,
    width: 300,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    // marginTop: 20,
    marginBottom: 20,
    borderRadius: 45
  },
  buttonText: {
    fontFamily: 'ubuntuBold',
    color: WHITE,
    fontSize: 15
  }
};

export default Register;
