import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, Image, Switch, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker';
import { CheckboxField } from 'react-native-checkbox-field';
import { BLACK, WHITE, ORANGE, HEADER_GREY, BROWN } from '../assets/Colors';

const WIDTH = Dimensions.get('window').width;
class NewPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
      photography: true,
      cooking: false,
      bird_watching: true,
      wedding: false,
      cafe_exploring: true,
      foodie_travel: false,
      solo: false,
    };
  }
  selectCheckbox(n) {
    switch (n) {
      case 1:
        this.setState({
          photography: !this.state.photography
        });
        break;
      case 2:
        this.setState({
          cooking: !this.state.cooking
        });
        break;
      case 3:
        this.setState({
          bird_watching: !this.state.bird_watching
        });
        break;
      case 4:
        this.setState({
          wedding: !this.state.wedding
        });
        break;
      case 5:
        this.setState({
          cafe_exploring: !this.state.cafe_exploring
        });
        break;
      case 6:
        this.setState({
          foodie_travel: !this.state.foodie_travel
        });
        break;
      default:
        return null;

    }
  }
  render() {
    return (
      <ScrollView>
        <Image source={require('../assets/images/boat.jpg')} style={[styles.backgroundImg, { height: 200 }]}>
          <View style={{ padding: 15 }}>
            <View style={styles.marginBottom15}>
              <View style={styles.inputField}>
                <Icon name='map-marker' size={25} color={BROWN} />
                <TextInput
                  placeholder='Staring Point'
                  underlineColorAndroid={'rgba(0,0,0,0)'} style={[styles.input, { marginLeft: 5 }]}
                />
              </View>
            </View>
            <View style={styles.marginBottom15}>
              <View style={styles.inputField}>
                <Icon name='map-marker' size={25} color={BROWN} />
                <TextInput
                  placeholder='Ending Point'
                  underlineColorAndroid={'rgba(0,0,0,0)'} style={[styles.input, { marginLeft: 5 }]}
                />
              </View>
            </View>
            <View style={[styles.marginBottom15]}>

              <View style={[styles.inputField, { marginRight: 5, width: 150, alignSelf: 'center' }]}>
                <Icon name='calendar' size={20} color={BROWN} />
                <DatePicker
                  style={{ flex: 1 }}
                  date={this.state.date}
                  mode="date"
                  placeholder="DD-MM-YYYY"
                  format="DD-MM-YYYY"
                  minDate="01-05-2016"
                  maxDate="01-05-2019"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  onDateChange={(date) => { this.setState({ date }); }}
                  customStyles={{
                    dateInput: {
                      borderWidth: 0,
                    },
                    dateText: {
                      fontFamily: 'ubuntuMedium'
                    }
                  }}
                />
              </View>

            </View>
          </View>
        </Image>

        <Image
          source={require('../assets/images/CreatePlanFooter.jpg')}
          style={[styles.backgroundImg, { height: 370 }]}
        >
          <View style={styles.soloSwitchContainer}>
            <Text style={{ fontFamily: 'ubuntuMedium', color: BLACK }}>SOLO</Text>
            <Switch
              onTintColor={'rgba(243,112,33,.5)'}
              thumbTintColor={ORANGE}
              onValueChange={(value) => { this.setState({ solo: value }); }}
              value={this.state.solo}
            />
          </View>
          <View>
            <Text style={styles.label}>TYPE OF INTEREST (select hightlight)</Text>
            <ScrollView style={{ marginTop: 10 }} horizontal showsHorizontalScrollIndicator={false}>
              <TouchableOpacity onPress={() => { this.selectCheckbox(1); }} activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/new-york.jpg')}
                  style={[styles.typeSelectionImage, { borderColor: this.state.photography ? ORANGE : 'transparent' }]}
                >
                  <View style={styles.selectionBox}>
                    <Text style={styles.checkText}>
                      PHOTOGRAPHY
                    </Text>
                    <Text style={styles.checkText}>&</Text>
                    <Text style={styles.checkText}>SIGHT SEEING</Text>
                  </View>
                </Image>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.selectCheckbox(2); }} activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/new-york.jpg')}
                  style={[styles.typeSelectionImage, { borderColor: this.state.cooking ? ORANGE : 'transparent' }]}
                >
                  <View style={styles.selectionBox}>
                    <Text style={styles.checkText}>
                      FOODIE TRAVEL
                    </Text>
                  </View>
                </Image>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.selectCheckbox(5); }} activeOpacity={0.9}>
                <Image
                  source={require('../assets/images/new-york.jpg')}
                  style={[styles.typeSelectionImage, { borderColor: this.state.cafe_exploring ? ORANGE : 'transparent' }]}
                >
                  <View style={styles.selectionBox}>
                    <Text style={styles.checkText}>
                      CAFE & CHILL
                    </Text>
                  </View>
                </Image>
              </TouchableOpacity>

            </ScrollView>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.button}>
            <Text style={styles.buttonText}>CREATE PLAN</Text>
          </TouchableOpacity>
        </Image>

      </ScrollView>
    );
  }
}
const styles = {
  marginBottom15: {
    marginBottom: 15
  },
  inputField: {
    height: 40,
    borderWidth: 1,
    borderColor: HEADER_GREY,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: WHITE,
    borderRadius: 25
  },
  input: {
    flex: 1,
    fontFamily: 'ubuntuMedium'
  },
  backgroundImg: {
    width: WIDTH,
    padding: 15,
    resizeMode: 'cover',
  },
  soloSwitchContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
    marginVertical: 20
  },
  // scroll: {
  //   borderWidth: 1,
  //   borderColor: BLACK,
  // },
  // checkContainer: {
  //     borderBottomWidth: 1,
  //     borderColor: HEADER_GREY,
  //     flexDirection: 'row',
  //     alignItems: 'center',
  //     padding: 0
  //   },
  //   checkboxStyle: {
  //     width: 20,
  //     height: 20,
  //     borderWidth: 2,
  //     borderColor: ORANGE,
  //     borderRadius: 20 / 2,
  //     marginVertical: -20,
  //     marginLeft: -7
  //   },
    typeSelectionImage: {
      marginRight: 10,
      width: 130,
      height: 85,
      borderWidth: 2,
      borderRadius: 5,
    },
    checkText: {
      fontSize: 12,
      color: WHITE,
      fontFamily: 'ubuntuRegular',
    },
    label: {
      fontSize: 13,
      fontFamily: 'ubuntuMedium',
      marginBottom: 5,
      color: BLACK
    },
    selectionBox: {
      backgroundColor: 'rgba(0,0,0,0.6)',
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      justifyContent: 'center',
      alignItems: 'center',
    },
    button: {
      backgroundColor: ORANGE,
      width: 300,
      height: 45,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      marginTop: 30,
      borderRadius: 45 / 2,
      marginBottom: 70
    },
    buttonText: {
      fontFamily: 'ubuntuBold',
      color: WHITE,
      fontSize: 15
    }
};

export default NewPlan;
