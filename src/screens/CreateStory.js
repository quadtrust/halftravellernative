import React, { Component } from 'react';
import { View, Text, TextInput, ScrollView, TouchableOpacity, Linking, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
// import DatePicker from 'react-native-datepicker';
import ImageCarousel from 'react-native-image-carousel';
import { BLACK, WHITE, ORANGE, HEADER_GREY, LEMON_GREEN } from '../assets/Colors';

const urls = [
  'https://d919ce141ef35c47fc40-b9166a60eccf0f83d2d9c63fa65b9129.ssl.cf5.rackcdn.com/images/67003.max-620x600.jpg',
  'https://d919ce141ef35c47fc40-b9166a60eccf0f83d2d9c63fa65b9129.ssl.cf5.rackcdn.com/images/51681.max-620x600.jpg',
  'https://d919ce141ef35c47fc40-b9166a60eccf0f83d2d9c63fa65b9129.ssl.cf5.rackcdn.com/images/66812.max-620x600.jpg',
  'https://myanimelist.cdn-dena.com/s/common/uploaded_files/1438960604-925d1997518b66f8508c749f36810793.jpeg',
];

class CreateStory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      date: ''
    };
  }
  _imageCarousel: ImageCarousel;
  _renderContent = (idx) => {
    return (
      <Image
        style={styles.container}
        source={{ uri: urls[idx] }}
        // resizeMode={'contain'}
      />
    );
  }
  render() {
    return (
      <View>
        <ScrollView contentContainerStyle={{ padding: 15, backgroundColor: WHITE }}>
          <TextInput
            multiline
            onContentSizeChange={(event) => {
            this.setState({ height: event.nativeEvent.contentSize.height });
            // console.log(event.nativeEvent.contentSize);
            }}
            placeholder='Write your travel story here..'
            underlineColorAndroid={'transparent'}
            selectionColor={BLACK}
            autoCapitalize={'sentences'}
            numberOfLines={25}
            textAlignVertical={'top'}
            style={[styles.inputStyle, { height: Math.max(100, this.state.height) }]}
          />

          <View style={styles.navigationContainer}>
            <TouchableOpacity style={{ marginRight: 10 }}>
              <Icon name='calendar' size={25} />

            </TouchableOpacity>
            <TouchableOpacity style={{ marginRight: 10 }} onPress={() => Linking.openURL('https://github.com/facebook/react-native/issues/13506')}>
              <Icon name='map-marker' size={25} />
            </TouchableOpacity>
            <TouchableOpacity style={{ marginRight: 10 }}>
              <Icon name='camera-retro' size={25} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.createButton}
            >
              <Text style={styles.buttonText}>CREATE</Text>
            </TouchableOpacity>
          </View>


          <View>
            <Text style={styles.timeText}>4 hrs ago</Text>
            <View style={{ flexDirection: 'row', marginBottom: 20 }}>
              <Icon name='car' size={25} color={BLACK} />
              <View style={{ paddingHorizontal: 10 }}>
                <Text style={styles.smallText}>
                  <Text style={styles.userName}>Sankalp Singha </Text>
                  has started his journey from Kolkata
                </Text>
                <Text style={styles.smallText}>12 July 2016</Text>
                <View style={styles.storyContent}>
                  <Text style={styles.smallText}>
                    I have started my journey to the scotland of the east i.e
                    Shillong and I hope that this would be a hell of a journey
                  </Text>
                  <View style={{ marginTop: 10 }}>
                    <ImageCarousel
                      ref={(imageCarousel: ImageCarousel) => {
                        this._imageCarousel = imageCarousel;
                      }}
                      renderContent={this._renderContent}
                      // renderHeader={this._renderHeader}
                      // renderFooter={this._renderFooter}
                    >
                    {urls.map((url) => (
                      <Image
                        key={url}
                        style={styles.image}
                        source={{ uri: url }}
                        // resizeMode={'contain'}
                      />
                    ))}
                  </ImageCarousel>
                  </View>
                </View>

              </View>
            </View>

            <View style={{ flexDirection: 'row', marginBottom: 20 }}>
              <Icon name='car' size={25} color={BLACK} />
              <View style={{ paddingHorizontal: 10 }}>
                <Text style={styles.smallText}>
                  <Text style={styles.userName}>Sankalp Singha </Text>
                  has started his journey from Kolkata
                </Text>
                <Text style={styles.smallText}>12 July 2016</Text>
                <View style={styles.storyContent}>
                  <Text style={styles.smallText}>
                    I have started my journey to the scotland of the east i.e
                    Shillong and I hope that this would be a hell of a journey
                  </Text>
                  <View style={{ marginTop: 10 }}>
                    <ImageCarousel
                      ref={(imageCarousel: ImageCarousel) => {
                        this._imageCarousel = imageCarousel;
                      }}
                      renderContent={this._renderContent}
                      // renderHeader={this._renderHeader}
                      // renderFooter={this._renderFooter}
                    >
                    {urls.map((url) => (
                      <Image
                        key={url}
                        style={styles.image}
                        source={{ uri: url }}
                      />
                    ))}
                  </ImageCarousel>
                  </View>
                </View>

              </View>
            </View>

          </View>
        </ScrollView>
        <TouchableOpacity style={styles.greenCircle}>
          <Icon name='check' color={WHITE} size={25} />
        </TouchableOpacity>

      </View>

    );
  }
}

const styles = {
  inputStyle: {
    borderColor: HEADER_GREY,
    borderWidth: 1,
    fontFamily: 'ubuntuRegular',
    padding: 7
  },
  timeText: {
    fontSize: 12,
    alignSelf: 'flex-end',
    fontFamily: 'ubuntuRegular',
    color: HEADER_GREY
  },
  navigationContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    marginVertical: 15
  },
  createButton: {
    backgroundColor: ORANGE,
    width: 90,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2
  },
  buttonText: {
    color: WHITE,
    fontFamily: 'ubuntuMedium',
    fontSize: 12
  },
  storyContent: {
    borderLeftWidth: 1,
    borderColor: HEADER_GREY,
    marginLeft: -23,
    paddingHorizontal: 23,
    marginTop: 10
  },
  image: {
    marginRight: 7,
    height: 90,
    width: 90
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  userName: {
    fontFamily: 'ubuntuBold',
    color: BLACK,
    fontSize: 15
  },
  smallText: {
    fontFamily: 'ubuntuRegular',
    fontSize: 12
  },
  greenCircle: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 20,
    alignSelf: 'flex-end',
    position: 'absolute',
    bottom: 20,
    right: 20,
    backgroundColor: LEMON_GREEN
  },
};
export default CreateStory;
