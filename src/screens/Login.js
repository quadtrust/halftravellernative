import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity,
   LayoutAnimation, TextInput, DeviceEventEmitter } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { LIGHT_WHITE, WHITE, FACEBOOK_BACKGROUND, GOOGLE_BACKGROUND,
   WHITE_OPACITY, ORANGE } from '../assets/Colors.js';


const SCREEN_WIDTH = Dimensions.get('window').width;

const SCREEN_HEIGHT = Dimensions.get('window').height;
const CONTAINER_HEIGHT = SCREEN_HEIGHT * 0.90;

const CustomLayoutSpring = {
    duration: 500,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.scaleXY,
      springDamping: 0.7,
    },
    update: {
      type: LayoutAnimation.Types.linear,
      springDamping: 0.7,
    },
  };

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loginContainerHeight: CONTAINER_HEIGHT,
        socialButtonContainerHeight: CONTAINER_HEIGHT * 0.30,
        socialButtonContainerOpacity: 1,
        partitionViewHeight: CONTAINER_HEIGHT * 0.10,
        partitionViewOpacity: 1
    };
  }
  componentWillMount() {
    this.keyboardDidShowListener = DeviceEventEmitter.addListener('keyboardDidShow',
                                                          this.keyboardDidShow.bind(this));
    this.keyboardDidHideListener = DeviceEventEmitter.addListener('keyboardDidHide',
                                                          this.keyboardDidHide.bind(this));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  keyboardDidShow() {
    this.setState({
      loginContainerHeight: undefined,
      socialButtonContainerHeight: 0,
      socialButtonContainerOpacity: 0,
      partitionViewHeight: 0,
      partitionViewOpacity: 0
    });
    LayoutAnimation.configureNext(CustomLayoutSpring);
  }

  keyboardDidHide() {
    this.setState({
      loginContainerHeight: CONTAINER_HEIGHT,
      socialButtonContainerHeight: CONTAINER_HEIGHT * 0.30,
      socialButtonContainerOpacity: 1,
      partitionViewHeight: CONTAINER_HEIGHT * 0.10,
      partitionViewOpacity: 1
    });
    LayoutAnimation.configureNext(CustomLayoutSpring);
  }

  render() {
    return (
      <Image
      resizeMode="cover"
      source={require('../assets/images/login-background.jpg')}
      style={styles.backgroundImg}
      >

      <View style={[styles.loginContainer, { height: this.state.loginContainerHeight }]}>
        <View style={styles.logoContainer}>
            <Image source={require('../assets/images/logo.png')} style={styles.logo} />
        </View>

        <View
          style={[styles.socialButtonContainer,
             { height: this.state.socialButtonContainerHeight,
                opacity: this.state.socialButtonContainerOpacity }]}
        >

        <View
           style={[styles.socialButton,
            styles.facebookColor
            ]}
        >
        <TouchableOpacity style={styles.socialButtonPositionDesign}>
        <View style={[styles.socialLogo, styles.socialLogoPosition]}>
          <Icon name='facebook-official' style={styles.iconInSocialButton} />
        </View>
          <Text style={styles.socialButtonText}>
           Login with Facebook
          </Text>
        </TouchableOpacity>
        </View>

        <View
           style={[styles.socialButton,
            styles.googleColor
            ]}
        >
        <TouchableOpacity style={styles.socialButtonPositionDesign}>
        <View style={[styles.socialLogo, styles.socialLogoPosition]}>
          <Icon name='google-plus-square' style={styles.iconInSocialButton} />
        </View>
          <Text style={styles.socialButtonText}>
           Login with Google
          </Text>
        </TouchableOpacity>
        </View>

        <View
           style={[styles.socialButton,
            styles.registerColor
            ]}
        >
        <TouchableOpacity
        onPress={() => this.props.navigation.navigate('register')}
        style={styles.socialButtonPositionDesign}
        >
          <Text style={styles.socialButtonText}>
           REGISTER
          </Text>
        </TouchableOpacity>
        </View>


        </View>

        <View
          style={[styles.partitionView,
           { height: this.state.partitionViewHeight,
              opacity: this.state.partitionViewOpacity }]}
        >
          <View style={styles.whitePartitionLine} />
        </View>

        <View style={styles.loginInputForm}>
          <View style={[styles.loginContainView, { marginBottom: 5 }]}>
            <Text style={styles.whiteOpacityText}>username</Text>
            <View style={styles.loginInputView}>
            <TextInput
              underlineColorAndroid='transparent'
              placeholder={'username'}
              style={styles.inputField}
              placeholderTextColor={LIGHT_WHITE}
            />
            </View>
          </View>

          <View style={[styles.loginContainView, { marginBottom: 20 }]}>
            <Text style={styles.whiteOpacityText}>password</Text>
            <View style={styles.loginInputView}>
            <TextInput
              underlineColorAndroid='transparent'
              placeholder={'password'}
              secureTextEntry
              style={styles.inputField}
              placeholderTextColor={LIGHT_WHITE}
            />
            </View>
          </View>

          <View
          style={styles.loginButton}
          >
            <TouchableOpacity
              style={styles.makeAtCenter}
            >
            <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
          </View>
          <View
          style={styles.forgotPasswordView}
          >
            <TouchableOpacity
              style={styles.makeAtCenter}
            >
            <Text style={styles.forgotPasswordText}>forgot password</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      </Image>
    );
  }
}

const styles = {
  logoContainer: {
    height: CONTAINER_HEIGHT * 0.20,
    width: SCREEN_WIDTH,
    alignItems: 'center',
    justifyContent: 'center'
  },
  socialButtonContainer: {
    paddingTop: 10,
    justifyContent: 'space-between',
  },
  socialLogoPosition: {
     position: 'absolute',
     left: 18,
     top: 0
  },
  socialButtonPositionDesign: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  iconInSocialButton: {
    fontSize: 18,
    color: WHITE
  },
  whitePartitionLine: {
     height: 2,
     backgroundColor: WHITE_OPACITY
   },
  partitionView: {
    justifyContent: 'center',
    width: SCREEN_WIDTH * 0.70,
    paddingLeft: 20,
    paddingRight: 20
   },
  socialButtonText: {
   color: '#FEFFFF',
   fontFamily: 'ubuntuRegular'
  },
  loginInputForm: {
     height: CONTAINER_HEIGHT * 0.30,
     justifyContent: 'space-between'
  },
  loginContainView: {
    width: SCREEN_WIDTH * 0.70,
    height: 60,
    justifyContent: 'center'
  },
  loginInputView: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    borderRadius: 5,
    paddingLeft: 15,
    height: 40
  },
  loginButton: {
     width: SCREEN_WIDTH * 0.70,
     height: 40,
     justifyContent: 'center',
     marginBottom: 10,
     backgroundColor: '#2196F3',
     borderRadius: 24,
  },
  forgotPasswordView: {
    width: SCREEN_WIDTH * 0.70,
    height: 15,
    justifyContent: 'center',
    marginTop: 5,
  },
  makeAtCenter: {
    justifyContent: 'center',
     alignItems: 'center'
  },
  loginText: {
    color: '#FEFFFF',
    fontFamily: 'ubuntuMedium'
  },
  forgotPasswordText: {
    color: '#FEFFFF',
    fontFamily: 'ubuntuRegular',
    fontSize: 12
  },
  inputField: {
    borderWidth: 0,
    fontFamily: 'ubuntuRegular',
    color: LIGHT_WHITE,
    fontSize: 12
  },
  whiteOpacityText: {
    color: LIGHT_WHITE,
    marginLeft: 15,
    marginBottom: 5,
    fontSize: 15,
    fontFamily: 'ubuntuRegular'
  },
  googleColor: {
    backgroundColor: GOOGLE_BACKGROUND
  },
  facebookColor: {
    backgroundColor: FACEBOOK_BACKGROUND
  },
  registerColor: {
    backgroundColor: ORANGE
  },
  socialButton: {
    paddingTop: 12,
    paddingBottom: 12,
    borderRadius: 5,
    width: SCREEN_WIDTH * 0.70,
  },
  logo: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  backgroundImg: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginContainer: {
    width: SCREEN_WIDTH,
    alignItems: 'center'
  }
};

export default Login;
