import _ from 'lodash';
import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Text,
  Image,
  ScrollView
} from 'react-native';
import { DrawerItems } from 'react-navigation';

import { RED, WHITE, BLACK } from '../../assets/Colors';

const { width } = Dimensions.get('window');
const drawerWidth = width / 1.3;


class Drawer extends Component {
  render() {
    // console.log(Object.keys(routes));
     const { routes, navigation } = this.props;
     console.log(routes);
     console.log(Object.keys(routes));
     const arr = [];
     Object.keys(routes).map(o => {
       const myObj = {};
       myObj[o] = routes[o];
       arr.push(myObj);
     });

 console.log(arr);
    return (
      <ScrollView>
        <Image style={styles.profileCover} source={require('../../assets/images/drawercover.png')}>
          <Image style={styles.profileImage} source={require('../../assets/images/profile.png')} />
        </Image>
        <View style={{ marginVertical: 60 }}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.profileName}>Sanjita SIngha</Text>
          </View>
          <View style={{ marginVertical: 20 }}>

          {
              arr.map((item, index) => {
                console.log(Object.keys(item)[0]);
                const p = Object.keys(item)[0];
                return ((index < 0) ? null :
                  (<TouchableOpacity

                    key={index} style={{ marginBottom: 0 }}
                       onPress={() => { navigation.navigate(Object.keys(item)[0]); }}
                  >
                    <View style={styles.navigationTexContainer}>
                     <Text style={styles.navigationText}>{item[p].navigationOptions.drawerLabel}</Text>
                    </View>
                  </TouchableOpacity>)
                );
              })
          }
          </View>
          <TouchableOpacity activeOpacity={0.9}>
            <Text style={styles.buttonText}>LOG OUT</Text>
          </TouchableOpacity>
          <Text style={styles.versionText}>ver 1.0</Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  profileCover: {
    width: drawerWidth,
    height: 150,
    position: 'relative'
  },
  profileImage: {
    width: 90,
    height: 90,
    position: 'absolute',
    bottom: -50,
    left: drawerWidth / 3.5,
    borderRadius: 150 / 2,
    borderColor: WHITE,
    borderWidth: 3
  },
  profileName: {
    color: BLACK,
    fontFamily: 'ubuntuBold',

  },
  navigationTexContainer: {
   alignItems: 'center',
   padding: 15,
 },
 navigationText: {
   fontSize: 16,
   fontFamily: 'ubuntuRegular',
   color: BLACK
 },
 buttonStyle: {
   backgroundColor: RED,
   width: 100,
   height: 30,
   justifyContent: 'center',
   alignItems: 'center',
   borderRadius: 3,
   alignSelf: 'center',
   marginBottom: 20
 },
 buttonText: {
   color: 'red',
   fontSize: 17,
   fontFamily: 'ubuntuRegular',
   textAlign: 'center'
 },
 versionText: {
   fontFamily: 'ubuntuRegular',
   color: BLACK,
   textAlign: 'center',
   marginTop: 50,
   fontSize: 11
 },
});

export default Drawer;
