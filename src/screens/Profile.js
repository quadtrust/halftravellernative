import React, { Component } from 'react';
import { Text, View, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native';
import Ion from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import ProfileTab from '../components/profiletab/ProfileTab';
import AboutMe from '../components/profiletab/AboutMe';
import IconTabBar from '../components/profiletab/IconTabBar';
// import Buddies from '../components/Buddies';
import { BLACK, WHITE, PINK, BLUE, BROWN } from '../assets/Colors';

const WIDTH = Dimensions.get('window').width;

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab1H: null,
      tab2H: null,
      tab3H: null,
      tab: 0
    };
    this._getTabHeight = this._getTabHeight.bind(this);
  }
  _onChangeTab({ i }) {
    this.setState({ tab: i });
  }

  _getTabHeight(name, height) {
    // console.log('THis is height', height);
    console.log('coming from tab', name);
    if (name === 'tab1') { this.setState({ tab2H: height, tab1H: height }); }
    if (name === 'tab3') { this.setState({ tab3H: height }); }
  }

  _onContentSizeChange(width, height) {
    console.log('THis is height', height);
    if (this.state.tab === 0 && this.state.tab1H === this.state.tab2H) {
			this.setState({ tab1H: height });
		}
  }

  render() {
    let height;
		if (this.state.tab === 0) height = this.state.tab1H;
		if (this.state.tab === 1) height = this.state.tab2H;
		if (this.state.tab === 2) height = this.state.tab3H;
    // console.log('THis is height', height);
    return (
      <View>
        <ScrollView contentContainerStyle={{ backgroundColor: WHITE }} >
          <View style={{ height }}>

            <View style={{ backgroundColor: WHITE }}>
              <Image source={require('../assets/images/drawercover.png')} style={styles.coverImage} />

              <View style={styles.profileInfoContainer}>
                <Image source={require('../assets/images/profile.png')} style={styles.myImage} />
                <View style={{ marginTop: 45, marginLeft: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.profileName}>Sanjita Singha</Text>
                    <Icon name='check-circle' size={20} color={BLUE} />
                  </View>
                  <View style={styles.infoWrapper}>
                    <Icon name='home' color={BLACK} size={15} />
                    <Text style={styles.normalText}>
                      Lives in Place Name
                    </Text>
                  </View>
                  <View style={styles.infoWrapper}>
                    <View style={{ flexDirection: 'row', }}>
                      <Icon name='mobile' color={BLACK} size={20} />
                      <Text style={styles.normalText}>
                        12 345897
                      </Text>
                    </View>
                    <TouchableOpacity style={styles.followButton}activeOpacity={0.8}>
                      <Text style={styles.followText}>Follow</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>

              <View style={styles.followContainer}>
                <TouchableOpacity style={styles.center}>
                  <Text style={styles.followerNumberText}>255</Text>
                  <Text style={styles.travelHeading}>Followers</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.center}>
                  <Text style={styles.followerNumberText}>25</Text>
                  <Text style={styles.travelHeading}>Following</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.center}>
                  <Text style={styles.followerNumberText}>5</Text>
                  <Text style={styles.travelHeading}>Stories</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.center}>
                  <Text style={styles.followerNumberText}>7</Text>
                  <Text style={styles.travelHeading}>Groups</Text>
                </TouchableOpacity>
              </View>
            </View>
            <ScrollableTabView
              onChangeTab={this._onChangeTab.bind(this)}
              renderTabBar={() => <IconTabBar />}
              style={styles.tabBar}
              // tabBarUnderlineStyle={styles.underlineStyle}
              initialPage={0}
            >
              <ProfileTab tabLabel="map" getTabHeight={this._getTabHeight.bind(this)} navigation={this.props.navigation} />
              <ProfileTab tabLabel="users" getTabHeight={this._getTabHeight.bind(this)} />
              <AboutMe tabLabel="user-circle" getTabHeight={this._getTabHeight.bind(this)} />
            </ScrollableTabView>
          </View>
          <TouchableOpacity
            activeOpacity={0.9}
            style={styles.storyButton}
            onPress={() => this.props.navigation.navigate('createstory')}
          >
            <Text style={styles.buttonText}>CREATE A NEW STORY</Text>
          </TouchableOpacity>
        </ScrollView>

        <TouchableOpacity
          style={styles.createPlan}
          activeOpacity={0.8}
          onPress={() => this.props.navigation.navigate('newplan')}
        >
          <Ion name="ios-add" size={35} color={WHITE} />
        </TouchableOpacity>

      </View>

    );
  }
}

const styles = {
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileInfoContainer: {
    marginTop: -40,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15
  },
  coverImage: {
    width: WIDTH,
    height: 200
  },
  myImage: {
    width: 90,
    height: 90,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: WHITE
  },
  infoWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 2
  },
  profileName: {
    fontSize: 15,
    fontFamily: 'ubuntuBold',
    color: BLACK,
    paddingRight: 20
  },
  normalText: {
    fontSize: 13,
    paddingLeft: 7,
    fontFamily: 'ubuntuRegular'
  },
  followContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
    paddingHorizontal: 30
  },
  storyButton: {
    backgroundColor: BROWN,
    width: 200,
    height: 45,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
    alignSelf: 'center'
  },
  buttonText: {
    color: WHITE,
    fontFamily: 'ubuntuBold',
    fontSize: 13
  },
  travelHeading: {
    fontFamily: 'ubuntuBold',
    color: BLACK
  },
  createPlan: {
    width: 50,
    height: 50,
    backgroundColor: PINK,
    borderRadius: 50 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 40,
    right: 10,
    elevation: 50
  },
  followerNumberText: {
    fontSize: 13,
    fontFamily: 'ubuntuBold',
    color: BLUE
  },
  tabBar: {
    backgroundColor: WHITE,

  },
  underlineStyle: {
    backgroundColor: BLUE,
  },
  followButton: {
    width: 90,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BLUE,
    marginLeft: 50
  },
  followText: {
    fontSize: 13,
    color: WHITE,
    fontFamily: 'ubuntuMedium'
  }
};

export default Profile;
