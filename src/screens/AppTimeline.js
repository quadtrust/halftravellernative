import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Notifications from '../components/Notifications';
import TravelStory from '../components/TravelStory';
import Buddies from '../components/Buddies';
import { BLACK, WHITE, ORANGE } from '../assets/Colors';

const nav = TabNavigator({
  travelstory: {
    screen: TravelStory,
    navigationOptions: {
      tabBarLabel: <View style={{ flexDirection: 'row' }}>
        <Icon name="home" size={20} color={WHITE} />
        <Text style={{ paddingLeft: 10, color: WHITE, fontFamily: 'ubuntuMedium' }}>HOME</Text>
      </View>
    }
  },

  buddies: {
    screen: Buddies
  },
  notifications: {
    screen: Notifications
  },
}, {
  tabBarOptions: {
    style: {
      backgroundColor: BLACK,
      paddingVertical: 5,
    },
    labelStyle: {
      fontFamily: 'ubuntuMedium',
      fontSize: 14
    },
    activeTintColor: WHITE,
    // showIcon: true
    indicatorStyle: {
      backgroundColor: ORANGE
    },
    tabStyle: {
      opacity: 1,
    }
  }
});


export default nav;
