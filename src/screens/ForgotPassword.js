import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity, LayoutAnimation, TextInput, DeviceEventEmitter, ScrollView } from 'react-native';

import { LIGHT_WHITE, WHITE, FACEBOOK_BACKGROUND, GOOGLE_BACKGROUND, WHITE_OPACITY } from '../assets/Colors.js';


const SCREEN_WIDTH = Dimensions.get('window').width;

const SCREEN_HEIGHT = Dimensions.get('window').height;
const CONTAINER_HEIGHT = SCREEN_HEIGHT * 0.67;

const CustomLayoutSpring = {
    duration: 500,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.scaleXY,
      springDamping: 0.7,
    },
    update: {
      type: LayoutAnimation.Types.linear,
      springDamping: 0.7,
    },
  };


class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginContainerHeight: CONTAINER_HEIGHT
    };
  }

  componentWillMount() {
    this.keyboardDidShowListener = DeviceEventEmitter.addListener('keyboardDidShow',
                                                          this.keyboardDidShow.bind(this));
    this.keyboardDidHideListener = DeviceEventEmitter.addListener('keyboardDidHide',
                                                          this.keyboardDidHide.bind(this));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  keyboardDidShow(e) {
    this.setState({
      loginContainerHeight: undefined
    });
    LayoutAnimation.configureNext(CustomLayoutSpring);
  }

  keyboardDidHide(e) {
    this.setState({
      loginContainerHeight: CONTAINER_HEIGHT,
    });
    LayoutAnimation.configureNext(CustomLayoutSpring);
  }

  render() {
    return (
      <Image
      resizeMode="cover"
      source={require('../assets/images/login-background.jpg')}
      style={styles.backgroundImg}
      >

      <View style={[styles.loginContainer, { height: this.state.loginContainerHeight }]}>
        <View style={styles.logoContainer}>
          <View style={styles.logoContainerInner}>
          <View style={styles.logoBackground}>
            <Text style={styles.halfText}>HALF</Text>
          </View>
          <Text style={styles.travellerText}>Traveller</Text>
          </View>
        </View>


        <View style={styles.loginInputForm}>
          <View style={[styles.loginContainView]}>
            <Text style={styles.whiteOpacityText}>enter your email</Text>
            <View style={styles.loginInputView}>
            <TextInput
              keyboardType={'email-address'}
              underlineColorAndroid='transparent'
              placeholder={'email'}
              style={styles.inputField}
              placeholderTextColor={LIGHT_WHITE}
            />
            </View>
          </View>

          <View
          style={styles.loginButton}
          >
            <TouchableOpacity
              style={styles.makeAtCenter}
            >
            <Text style={styles.loginText}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      </Image>
    );
  }
}

const styles = {
  logoContainer: {
    height: CONTAINER_HEIGHT * 0.15,
    justifyContent: 'flex-start'
  },
  logoContainerInner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  socialButtonContainer: {
    justifyContent: 'space-between'
  },
  socialLogoPosition: {
     position: 'absolute',
     left: 18,
     top: 0
  },
  socialButtonPositionDesign: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  iconInSocialButton: {
    fontSize: 18,
    color: WHITE
  },
  whitePartitionLine: {
     height: 2,
     backgroundColor: WHITE_OPACITY
   },
  partitionView: {
    justifyContent: 'center',
    width: SCREEN_WIDTH * 0.70,
    paddingLeft: 20,
    paddingRight: 20
   },
  socialButtonText: {
   color: '#FEFFFF',
   fontFamily: 'ubuntuRegular'
  },
  loginInputForm: {
     height: CONTAINER_HEIGHT * 0.33,
     justifyContent: 'space-between'
  },
  loginContainView: {
    width: SCREEN_WIDTH * 0.70,
    height: 60,
    justifyContent: 'center'
  },
  loginInputView: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    borderRadius: 5,
    paddingLeft: 15,
    height: 40
  },
  loginButton: {
     width: SCREEN_WIDTH * 0.70,
     height: 40,
     justifyContent: 'center',
     marginBottom: 10,
     backgroundColor: '#2196F3',
     borderRadius: 24,
  },
  forgotPasswordView: {
    width: SCREEN_WIDTH * 0.70,
    height: 30,
    justifyContent: 'center',
    marginTop: 10,
  },
  makeAtCenter: {
    justifyContent: 'center',
     alignItems: 'center'
  },
  loginText: {
    color: '#FEFFFF',
    fontFamily: 'ubuntuMedium'
  },
  inputField: {
    borderWidth: 0,
    fontFamily: 'ubuntuRegular',
    color: LIGHT_WHITE,
    fontSize: 12
  },
  whiteOpacityText: {
    color: LIGHT_WHITE,
    marginLeft: 15,
    marginBottom: 5,
    fontSize: 15,
    fontFamily: 'ubuntuRegular'
  },
  googleColor: {
    backgroundColor: GOOGLE_BACKGROUND
  },
  facebookColor: {
    backgroundColor: FACEBOOK_BACKGROUND
  },
  socialButton: {
    paddingTop: 12,
    paddingBottom: 12,
    borderRadius: 5,
    width: SCREEN_WIDTH * 0.70,
  },
  logoBackground: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 12,
    paddingRight: 12,
    borderRadius: 5,
    backgroundColor: '#EEEEEE',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 2
  },
  halfText: {
    fontSize: 20,
    fontFamily: 'ubuntuBold',
    color: '#D5ACAD'
  },
  travellerText: {
    fontSize: 14,
    fontFamily: 'ubuntuMedium',
    color: '#eee'
  },
  backgroundImg: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginContainer: {
    width: SCREEN_WIDTH,
    alignItems: 'center'
  }
};

export default ForgotPassword;
