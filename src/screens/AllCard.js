import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TextInput, Dimensions  } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Card from '../components/Card.js';
import SearchBarIcon from '../components/SearchBarAndIcon.js';
import { SLATE_BLUE, WHITE } from '../assets/Colors.js';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SEARCHBAR_HEIGHT = SCREEN_HEIGHT * 0.10;
const IMG_HEIGHT = SCREEN_HEIGHT * 0.25;
const PADDING_HEIGHT = SCREEN_HEIGHT * 0.04;
const CARD_DESCRIPTION_HEIGHT = SCREEN_HEIGHT * 0.47;

class AllCard extends Component {
  render() {
    return (
      <View
        style={styles.fullContainer}
      >
      <SearchBarIcon height={SEARCHBAR_HEIGHT} />
        <View style={styles.cardView}>
          <Card imgheight={IMG_HEIGHT} descheight={CARD_DESCRIPTION_HEIGHT} paddingheight={PADDING_HEIGHT} />
        </View>
        <View style={{ height: PADDING_HEIGHT }} />
      </View>
    );
  }
}
const styles = {
  fullContainer: {
    backgroundColor: SLATE_BLUE,
    flex: 1
  },
  cardView: {
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default AllCard;
