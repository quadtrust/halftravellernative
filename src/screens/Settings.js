import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Switch } from 'react-native';
// import { CheckboxField } from 'react-native-checkbox-field';
import { BLACK, WHITE, ORANGE, HEADER_GREY } from '../assets/Colors';


class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: true,
      vibration: false,
      background_data: false,
      travel_story: true
    };
  }

  switchToggle(val, i) {
    switch (i) {
      case 1:
        this.setState({
          notifications: val
        });
        break;
      case 2:
        this.setState({
          travel_story: val
        });
        break;
      case 3:
        this.setState({
          vibration: val
        });
        break;
      case 4:
        this.setState({
          background_data: val
        });
        break;
      default:
        return null;

    }
  }
  render() {
    return (
      <View style={styles.body}>
        <Text style={styles.headingText}>General</Text>
        <TouchableOpacity
          style={[styles.checkContainer]}
          activeOpacity={0.9}
        >

          <Text style={[styles.checkText]}>
            Notifications
          </Text>
          <Switch
            onTintColor={'rgba(243,112,33,.5)'}
            thumbTintColor={ORANGE}
            // tintColor={ORANGE}
            onValueChange={(value) => { this.switchToggle(value, 1); }}
            value={this.state.notifications}
            style={{ flex: 1 }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.checkContainer]}
          activeOpacity={0.9}
        >

          <Text style={[styles.checkText]}>
            Sounds in the App
          </Text>
          <Switch
            onTintColor={'rgba(243,112,33,.5)'}
            thumbTintColor={ORANGE}
            // tintColor={ORANGE}
            onValueChange={(value) => { this.switchToggle(value, 2); }}
            value={this.state.travel_story}
            style={{ flex: 1 }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.checkContainer]}
          activeOpacity={0.9}
        >

          <Text style={[styles.checkText]}>
            Vibration
          </Text>
          <Switch
            onTintColor={'rgba(243,112,33,.5)'}
            thumbTintColor={ORANGE}
            // tintColor={ORANGE}
            onValueChange={(value) => { this.switchToggle(value, 3); }}
            value={this.state.vibration}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.checkContainer]}
          activeOpacity={0.9}
        >

          <Text style={[styles.checkText]}>
            Links open externally
          </Text>
          <Switch
            onTintColor={'rgba(243,112,33,.5)'}
            thumbTintColor={ORANGE}
            // tintColor={'green'}
            onValueChange={(value) => { this.switchToggle(value, 4); }}
            value={this.state.background_data}
            style={{ flex: 1 }}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.passwordButton} activeOpacity={0.8}>
          <Text style={styles.buttonText}>CHANGE YOUR PASSWORD</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={{ color: ORANGE, fontFamily: 'ubuntuRegular', alignSelf: 'center' }}>
            CLEAR YOUR PHONE'S BROWSING DATA
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  body: {
    backgroundColor: WHITE,
    flex: 1,
    paddingVertical: 20
  },
  headingText: {
    fontSize: 13,
    fontFamily: 'ubuntuRegular',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  checkContainer: {
    borderBottomWidth: 1,
    borderColor: HEADER_GREY,
    paddingBottom: 8,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between',
    paddingHorizontal: 10,
  },

  checkText: {
    fontSize: 15,
    color: BLACK,
    fontFamily: 'ubuntuMedium',
    flex: 5
  },
  passwordButton: {
    borderColor: ORANGE,
    borderRadius: 25,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 50,
    alignSelf: 'center',
    marginVertical: 30
  },
  buttonText: {
    color: ORANGE,
    fontSize: 15,
    fontFamily: 'ubuntuRegular'
  }
};

export default Settings;
