import { StackNavigator, DrawerNavigator } from 'react-navigation';
import React, { Component } from 'react';
import { Dimensions, View, TouchableOpacity, Modal, TouchableHighlight, Text, TextInput }
from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { LIGHT_GREY, ORANGE, WHITE, BLACK } from './assets/Colors';
import AppTimeline from './screens/AppTimeline';
import Login from './screens/Login.js';
import ForgotPassword from './screens/ForgotPassword.js';
import TravelDetails from './screens/TravelDetails';
import DrawerContent from './screens/drawer';
import MyProfile from './screens/MyProfile';
import Settings from './screens/Settings';
import NewPlan from './screens/NewPlan';
import Register from './screens/Register';
import CreateStory from './screens/CreateStory';
import Profile from './screens/Profile';

const { width } = Dimensions.get('window');
const mainDrawerRoutes = {
  apptimeline: {
    screen: AppTimeline,
    navigationOptions: {
    drawerLabel: 'Home',
    }
  },
  login: {
    screen: Login,
    navigationOptions: {
      header: null,
      drawerLabel: 'Login',
    },
  },
  myprofile: {
    screen: MyProfile,
    navigationOptions: {
    drawerLabel: 'My Profile',
    }
  },
  settings: {
    screen: Settings,
    navigationOptions: {
    drawerLabel: 'App Settings',

    }
  },
  createstory: {
    screen: CreateStory,
    navigationOptions: {
      title: 'Create Story',
      drawerLabel: 'Create a New Story'
    },
  }
};
let status = false;
const toggleDrawer = (navigation) => {
  console.log('PRESSED...');
    // const { navigation } = this.props;
    console.log(navigation.state);
    // const { routes, index } = navigation.state;
    if (!status) {
      navigation.navigate('DrawerOpen');
      status = true;
    } else {
      navigation.navigate('DrawerClose');
      status = false;
    }
  };

const mainNavigation = DrawerNavigator({ ...mainDrawerRoutes }, {
  drawerWidth: width / 1.4,
  contentComponent:
  ({ navigation }) => <DrawerContent navigation={navigation} routes={mainDrawerRoutes} />,

});


const MenuButton = (navigation) => {
  return (
    <View>
      <TouchableOpacity onPress={() => { toggleDrawer(navigation); }}>
        <Icon name="bars" style={{ color: WHITE, padding: 10, marginLeft: 10, fontSize: 20 }} />
      </TouchableOpacity>
     </View>
  );
};

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
  }
  componentDidMount() {
    console.log('search COmponent');
  }
  setModalVisible(visible) {
       this.setState({ modalVisible: visible });
   }
   openModal() {
     console.log('modal Open');
     this.setModalVisible(true);
   }
  render() {
    return (
      <View>
        <TouchableOpacity onPress={this.openModal.bind(this)}>
          <Icon name="search" style={styles.searchIcon} />
        </TouchableOpacity>
        <Modal
         animationType={'slide'}
         transparent
         visible={this.state.modalVisible}
         onRequestClose={() => this.setModalVisible(!this.state.modalVisible)}
        >
            <View style={[styles.modalContainer]}>
              <View>
               <View style={styles.modalHeader}>
                <Text style={styles.headerText}>Search</Text>
                 <TouchableHighlight
                  onPress={() => {
                   this.setModalVisible(!this.state.modalVisible);
                  }}
                 >
                  <Icon name={'close'} color={'white'} size={25} />
                 </TouchableHighlight>
                </View>
                <View style={styles.inputContainer}>
                  <View style={styles.inputField}>
                    <Icon name='search' size={25} color={BLACK} style={{ marginLeft: 10 }} />
                    <TextInput
                      placeholder='Search something here...'
                      keyboardType={'web-search'}
                      underlineColorAndroid={'rgba(0,0,0,0)'}
                      style={{ flex: 1, marginLeft: 5, fontFamily: 'ubuntuRegular' }}
                    />
                  </View>
                </View>
              </View>
            </View>
           </Modal>
       </View>
    );
  }
}

const landing = StackNavigator({
  main: {
    screen: mainNavigation,
    navigationOptions: ({ navigation }) => ({
      title: 'App',
      headerTintColor: 'white',
      headerLeft: <MenuButton navigate={navigation.navigate} />,
    }),
  },
  register: {
    screen: Register,
    navigationOptions: {
     header: null,
     },
  },
  login: {
    screen: Login,
    navigationOptions: {
      header: null,
    },
  },
  forgotpassword: {
    screen: ForgotPassword,
    navigationOptions: {
      header: null,
      // drawerLockMode: 'locked'
    },
  },
  createstory: {
    screen: CreateStory,
    navigationOptions: () => ({
      title: 'Create Story',
    }),
  },
  traveldetails: {
    screen: TravelDetails,
  },
  newplan: {
    screen: NewPlan,
    navigationOptions: () => ({
      title: 'Create plan',
      headerTintColor: 'white',
    }),
  },
  profile: {
    screen: Profile
  }
}
, { navigationOptions: ({ navigation }) => ({
		headerTintColor: 'white',
		headerStyle: styles.header,
    headerRight: <Search navigate={navigation.navigate} />
	}), }
);

const styles = {
  header: {
    height: 50,
    backgroundColor: ORANGE
  },
  modalContainer: {
   backgroundColor: 'rgba(255,255,255,.9)',
   flex: 1
  },
  headerText: { color: WHITE, fontFamily: 'ubuntuMedium', fontSize: 17 },
  inputContainer: {
    backgroundColor: BLACK,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15
  },
  inputField: {
  flexDirection: 'row',
  height: 40,
  backgroundColor: LIGHT_GREY,
  borderRadius: 17,
  justifyContent: 'center',
  alignItems: 'center'
},
  searchIcon: {
    color: 'white',
    padding: 10,
    marginLeft: 10,
    fontSize: 20
  },
  modalHeader: {
   flexDirection: 'row',
   justifyContent: 'space-between',
   alignItems: 'center',
   backgroundColor: ORANGE,
   height: 50,
   paddingHorizontal: 20
  },
};

export default landing;
