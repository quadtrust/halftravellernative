export const WHITE = '#fff';
export const YELLOW = '#F89E20';
export const GREEN = '#129C8E';
export const RED = '#9C1325';
export const BLUE = '#2196F3';
export const PINK = '#E23F77';
export const BLACK = '#2E3A44';
export const LIGHT_GREY = '#F0EFEF';
export const SLATE_BLUE = '#224455';
export const FACEBOOK_BACKGROUND = '#3B5998';
export const TWITTER_BACKGROUND = '#00ACEE';
export const GOOGLE_BACKGROUND = '#DD4B39';
export const INSTAGRAM_BACKGROUND = '#3F729B';
export const WHITE_OPACITY = 'rgba(200, 200, 200, 0.5)';
export const LIGHT_WHITE = '#c2c2c2';
export const PURPLE = '#9013FE';
export const HEADER_GREY = '#D7D7D7';
export const ORANGE = '#F37021';
export const BROWN = '#CC6D34';
export const LEMON_GREEN = '#4BA022';
export const BODY_COLOR = '#ECEAEA';
